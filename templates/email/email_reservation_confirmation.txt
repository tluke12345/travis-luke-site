こんにちは{% if reservation.name %}{{ reservation.name }}様{% endif %}。
ご予約ありがとうございます。詳細は以下のとおりです。

{% include 'email/includes/email_reservation_summary.txt' %}

予約の最新状況やキャンセルはここ：
www.travis-luke.com/ja/reservation/{{ reservation.pk }}/?token={{ reservation.token }}

よくある質問：
www.travis-luke.com/ja/faq/


よろしくお願いします。
Travis Luke
www.travis-luke.com


※当カフェからのメール配信が不要な方はこちらにアクセスして下さい。
[UNSUBSCRIBE]