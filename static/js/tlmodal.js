const TLModal = (() => {
  "use strict";

  /*
  USAGE
  ------------------------------------------------------------------------------
  - Include tlmodal.js
  - Include tlmodal.css (or _tlmodal.scss)

  CSS
  ------------------------------------------------------------------------------
  Showing is triggered by adding 'is-open' css class to modal.root
  Hiding is triggered by removing 'is-open' css class from modal.root

  hiding animations is default 300ms (HIDING_ANIMATION_DURATION)


  
  HTML
  ------------------------------------------------------------------------------
  <!-- Modal -->
  <div class="tlmodal" id="modal1" aria-hidden="true">

    <!-- Modal Overlay (covers and darkens/blurs background) -->
    <div class="tlmodal__overlay" tabindex="-1">
  
      <!-- Modal Container (modal content) -->
      <div class="tlmodal__container" role="dialog" aria-modal="true" aria-labelledby="modal1-title">

        <header>
          <h2 class="tlmodal__title" id="modal1-title">Title Here</h2>
          <button class="tlmodal__close" onclick="TLModal.hide()"></button>
        </header>
        <main>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque
          dignissimos voluptatibus, sapiente tenetur in mollitia animi qui unde
          omnis similique impedit officiis explicabo voluptas rem atque quasi
          accusamus inventore laudantium.
        </main>
        <footer>
          FOOTER HERE
        </footer>

      </div>
    </div>
  </div>

  SHOWING
  ------------------------------------------------------------------------------
  <button onclick="TLModal.show('MODAL_ID'); blur();">Show Modal</button>
  
  HIDING
  ------------------------------------------------------------------------------
  <button onclick="TlModal.hide('MODAL_ID'); blur();">Close</button>
   */

  // CONSTANTS -----------------------------------------------------------------
  const HIDE_ANIMATION_DURATION = 300;

  // STATE ---------------------------------------------------------------------
  let currentlyDisplayedModal = null;

  // UTILS ---------------------------------------------------------------------
  const removeClassFromElement = (element, className) => {
    var classes = element.className.split(" ");
    var newClassName = "";
    var i;
    for (i = 0; i < classes.length; i++) {
      if (classes[i] !== className) {
        newClassName += classes[i] + " ";
      }
    }
    element.className = newClassName;
  };
  const addClassToElement = (element, className) => {
    var arr = element.className.split(" ");
    if (arr.indexOf(className) == -1) {
      element.className += " " + className;
    }
  };
  const stopPropagation = (e) => {
    e.stopPropagation();
  };

  // UI ------------------------------------------------------------------------
  const displayModal = (modalToDisplay) => {
    // HIDE Current modal (if applicable)
    if (currentlyDisplayedModal) {
      removeClassFromElement(currentlyDisplayedModal.root, "is-open");
      setTimeout(function () {
        document.body.style.margin = "";
        document.body.style.overflow = "";
        // HIDDEN -> NOW recursive call to 'displayModal' to SHOW this time
        displayModal(modalToDisplay);
      }, HIDE_ANIMATION_DURATION);

      // Waiting for animations. SHOW will happen in recursive call
      currentlyDisplayedModal = null;
      return;
    }

    // DISPLAY modal
    if (modalToDisplay) {
      currentlyDisplayedModal = modalToDisplay;
      const scrollBarWidth = window.innerWidth - document.body.offsetWidth;
      document.body.style.margin = "0px " + scrollBarWidth + "px 0px 0px";
      document.body.style.overflow = "hidden";
      addClassToElement(modalToDisplay.root, "is-open");
    }
  };

  // SHOW ----------------------------------------------------------------------
  const show = (modalID) => {
    let modal = {
      events: [],
    };
    modal.root = document.getElementById(modalID);
    if (!modal.root) {
      console.log(`Unable to find Element with 'id="${modalID}" tag`);
      return;
    }

    modal.overlay = modal.root.firstElementChild;
    if (!modal.overlay) {
      console.log(
        `Modal Inproperly Formatted: modal first child must be overlay element`
      );
      return;
    }

    modal.container = modal.overlay.firstElementChild;
    if (!modal.container) {
      console.log(
        `Modal Inproperly Formatted: modal overlay first child must be container element`
      );
      return;
    }

    displayModal(modal);

    // Clicking to background overlay closes modal
    modal.overlay.addEventListener("click", hide);

    // Stop clicks in modal from bleeding through to overlay
    modal.container.addEventListener("click", (e) => {
      e.stopPropagation();
    });
  };

  // HIDE ----------------------------------------------------------------------
  const hide = () => {
    displayModal(null);
  };

  // ESCAPE KEY = CLOSE MODAL --------------------------------------------------
  document.addEventListener("keyup", (event) => {
    if (!currentlyDisplayedModal) return;

    event = event || window.event;
    let isEscape = false;
    if ("key" in event) {
      isEscape = event.key === "Escape" || event.key === "Esc";
    } else {
      isEscape = event.keyCode === 27;
    }
    if (isEscape) {
      hide();
    }
  });

  return {
    show: show,
    hide: hide,
  };
})();
