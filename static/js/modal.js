const Modal = (function () {
  "use strict";

  // CONSTANTS
  const ANIMATION_DURATION = 300;
  const MODAL = document.getElementById("modal");
  const TITLE_ID = "modal__title";
  let ROOT = location.href.split("?")[0].split("#")[0];
  const QUERY = location.href.substr(ROOT.length);
  const PAGE_CONTAINER = document.getElementsByClassName("page_container")[0];

  // Globals
  let STATE = {};
  let VISIBLE_ELEMENT = null;

  // UI UPDATE -------------------------------------------------------------------
  const updateTitleUI = function (text) {
    const title_el = document.getElementById(TITLE_ID);
    if (title_el) title_el.innerHTML = text;
  };
  const updateUI = function () {
    // Remove current modal if visible
    if (VISIBLE_ELEMENT) {
      PAGE_CONTAINER.removeClass("modal_blur");
      MODAL.removeClass("visible");
      const view = VISIBLE_ELEMENT;
      setTimeout(function () {
        view.removeClass("visible");
        updateTitleUI("");
        document.body.style.margin = "";
        document.body.style.overflow = "";
        updateUI(); // cleanup done, now call update to show current state
      }, ANIMATION_DURATION);
      VISIBLE_ELEMENT = null;
      return;
    }

    // Clean slate -> no modal on screen
    const modalId = STATE.modalId;
    const view = modalId ? document.getElementById(modalId) : null;

    // Useful message for debuging
    if (modalId && !view) console.error(`No modal exists for id '${modalId}'`);

    // No view -> nothing to show
    if (!view) return;

    // Configure and display Modal
    VISIBLE_ELEMENT = view;
    VISIBLE_ELEMENT.addClass("visible");
    if (STATE.modalTitle) updateTitleUI(STATE.modalTitle);
    const scrollBarWidth = window.innerWidth - document.body.offsetWidth;
    document.body.style.margin = "0px " + scrollBarWidth + "px 0px 0px";
    document.body.style.overflow = "hidden";
    PAGE_CONTAINER.addClass("modal_blur");
    MODAL.addClass("visible");
  };

  const show = function (modalId) {
    /* Show Modal 
      Displays the modal view. 
      Sets modal content element with id 'modalId' to visible.
      Sets modal title to 'modalTitle'
      if path: appends path to end of url
      updates browser history (allow for back/forward navigation)
    */
    // set current history state as modal
    const view = document.getElementById(modalId);
    if (!view) console.error(`No modal exists for id '${modalId}'`);

    const modalTitle = view.dataset.title;
    const path = view.dataset.path;

    STATE = {
      modalTitle: modalTitle,
      modalId: modalId,
    };

    let url = path ? ROOT + path + "/" : ROOT;
    history.pushState(STATE, null, url + QUERY);
    updateUI();
  };
  const hide = function () {
    history.back();
  };
  const initWithModal = function (modalId) {
    // use this method when:
    // you navigate directly to the modal url intstead of navigating to root url
    // ASSUMES:
    //  the last url path parameter length > 0

    // set current history state as modal
    const view = document.getElementById(modalId);
    if (!view) console.error(`No modal exists for id '${modalId}'`);

    const modalTitle = view.dataset.title;
    const path = view.dataset.path;

    STATE = {
      modalTitle: modalTitle,
      modalId: modalId,
    };

    // If path -> url changes when modal is shown
    if (path) {
      // our current ROOT is wrong, we need to remove the modal path
      ROOT = ROOT.substring(0, ROOT.lastIndexOf(path));
    }
    history.replaceState(STATE, null, null);
    updateUI();
  };

  // clicking outside the modal view closes it
  MODAL.addEventListener("click", function (e) {
    history.back();
  });

  // stop touches from bleeding through the modal views
  for (let i = 0; i < MODAL.children.length; i++) {
    MODAL.children[i].addEventListener("click", (event) => {
      event.stopPropagation();
    });
  }

  // Listen for window 'popstate' (back button) events
  window.addEventListener("popstate", function (event) {
    STATE = event.state || {};
    // Problem: when hitting button that calls history.back, the screen flashes
    // and returns to the same screen without actually going back
    //
    // when history.back() is called, this method updates the UI
    // It might update too fast... before the status is properly set
    // this seems to fix the problem
    this.setTimeout(() => {
      updateUI();
    }, 50);
  });

  // Hide modal if escape key is pressed
  document.addEventListener("keydown", function (event) {
    if (!VISIBLE_ELEMENT) return;

    event = event || window.event;
    let isEscape = false;
    if ("key" in event) {
      isEscape = event.key === "Escape" || event.key === "Esc";
    } else {
      isEscape = event.keyCode === 27;
    }
    if (isEscape) {
      history.back();
    }
  });

  return {
    hide: hide,
    show: show,
    initWithModal: initWithModal,
  };
})();
