let SELECTED = null;
const DAY = document.getElementById("id_day");

const handleDayClickEvent = function (day) {
  const day_id = "day_" + day;
  const el = document.getElementById(day_id);

  if (SELECTED && SELECTED != el) {
    // Remove previous selection
    SELECTED.removeClass("event_add_selected");
  }

  SELECTED = el;
  SELECTED.addClass("event_add_selected");
  DAY.value = day;
};

// INITIALIZE CALENDAR
if (DAY.value) {
  const day_id = "day_" + DAY.value;
  const el = document.getElementById(day_id);
  el.addClass("event_add_selected");
}
