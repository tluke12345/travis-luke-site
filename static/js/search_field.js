"use strict";

const clearSearch = function () {
  const el = document.getElementById("search_field_input");
  el.value = "";
  el.dispatchEvent(new Event("keyup"));
};
