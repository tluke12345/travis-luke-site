"use strict";

// Legacy support for css class add/remove
// ---------------------------------------------------------------------------
HTMLElement.prototype.addClass = function (className) {
  var arr = this.className.split(" ");
  if (arr.indexOf(className) == -1) {
    this.className += " " + className;
  }
};
HTMLElement.prototype.removeClass = function (className) {
  var classes = this.className.split(" ");
  var newClassName = "";
  var i;
  for (i = 0; i < classes.length; i++) {
    if (classes[i] !== className) {
      newClassName += classes[i] + " ";
    }
  }
  this.className = newClassName;
};

function removeElementsByClass(className) {
  var elements = document.getElementsByClassName(className);
  while (elements.length > 0) {
    elements[0].parentNode.removeChild(elements[0]);
  }
}

// Clipboard
// ---------------------------------------------------------------------------
function copyToClipboard(str) {
  const el = document.createElement("textarea");
  el.value = str;
  el.setAttribute("readonly", "");
  el.style.position = "absolute";
  el.style.left = "-9999px";
  document.body.append(el);
  const selected =
    document.getSelection().rangeCount > 0
      ? document.getSelection().getRangeAt(0)
      : false;
  el.select();
  document.execCommand("copy");
  document.body.removeChild(el);
  if (selected) {
    document.getSelection().removeAllRanges();
    document.getSelection().addRange(selected);
  }

  // Create Indicator
  alert = document.createElement("div");
  alert.appendChild(document.createTextNode("Copied!"));
  alert.id = "copied_alert";
  document.body.appendChild(alert);

  // Show animation and remove when done
  setTimeout(() => {
    alert.className = "show";
    setTimeout(() => {
      alert.className = "";
      setTimeout(() => {
        document.body.removeChild(alert);
      }, 50);
    }, 500);
  }, 200);
}

// Cookies (used to get csrf token)
// ---------------------------------------------------------------------------
// JavaScript function to get cookie by name; retrieved from https://docs.djangoproject.com/en/3.1/ref/csrf/
function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== "") {
    const cookies = document.cookie.split(";");
    for (let i = 0; i < cookies.length; i++) {
      const cookie = cookies[i].trim();
      // Does this cookie string begin with the name we want?
      if (cookie.substring(0, name.length + 1) === name + "=") {
        cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
        break;
      }
    }
  }
  return cookieValue;
}

// AJAX
// ---------------------------------------------------------------------------
// JavaScript wrapper function to send HTTP requests using Django's "X-CSRFToken" request header
function sendHttpAsync(path, method, body) {
  let props = {
    method: method,
    headers: {
      "X-CSRFToken": getCookie("csrftoken"),
    },
    mode: "same-origin",
  };

  if (body !== null && body !== undefined) {
    props.body = JSON.stringify(body);
  }

  return fetch(path, props)
    .then((response) => {
      return response.json().then((result) => {
        return {
          ok: response.ok,
          body: result,
        };
      });
    })
    .then((resultObj) => {
      return resultObj;
    })
    .catch((error) => {
      throw error;
    });
}

// URLS
// ---------------------------------------------------------------------------
function hostURI() {
  return window.location.protocol + "//" + window.location.host;
}

// SIZING
// ---------------------------------------------------------------------------
function resizeTextarea(textarea) {
  if (textarea.clientHeight < textarea.scrollHeight) {
    textarea.style.height = textarea.scrollHeight + 5 + "px";
  }
}
