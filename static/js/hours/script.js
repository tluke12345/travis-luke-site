"use strict";

const setActive = (element) => {
  const current = document.getElementsByClassName("active");
  for (let index = 0; index < current.length; index++) {
    const element = current[index];
    element.classList.remove("active");
  }
  if (element) {
    element.classList.add("active");
  }
};

const update_entry_date = (element) => {
  let current = document.getElementsByClassName("entry-week--selected");
  while (current.length)
      current[0].classList.remove("entry-week--selected");
  document.getElementById('id_datetime').value = element.dataset.value
  element.classList.add('entry-week--selected')
}