"use strict";

let CURRENT = null;
const toggle = function (expandId) {
  const selection = document.getElementById(expandId);

  // Selection is already expanded -> contract
  if (CURRENT == selection) {
    selection.removeClass("expanded");
    CURRENT = null;
  } else {
    // Another issue is expanded
    if (CURRENT) {
      CURRENT.removeClass("expanded");
      CURRENT = null;
    }

    // Exapnd selection
    if (selection) {
      selection.addClass("expanded");
      CURRENT = selection;
    }
  }
};
