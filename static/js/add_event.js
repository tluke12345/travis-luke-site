const SELECTED = [];
const DATE_SELECT = document.getElementById("id_date");

const handleDayClickEvent = function (day) {
  const day_id = "day_" + day;
  const el = document.getElementById(day_id);

  if (SELECTED.indexOf(day_id) != -1) {
    SELECTED.pop(day_id);
    el.removeClass("event_add_selected");

    for (let idx = 0; idx < DATE_SELECT.children.length; idx++) {
      const element = DATE_SELECT.children[idx];
      if (element.value == day) element.selected = false;
    }
  } else {
    SELECTED.push(day_id);
    el.addClass("event_add_selected");
    for (let idx = 0; idx < DATE_SELECT.children.length; idx++) {
      const element = DATE_SELECT.children[idx];
      if (element.value == day) element.selected = true;
    }
  }
};

// INITIALIZE CALENDAR
for (let idx = 0; idx < DATE_SELECT.children.length; idx++) {
  el = DATE_SELECT.children[idx]
  if (!el.selected) continue;
  
  day_id = `day_${el.value}`;
  day = document.getElementById(day_id)
  day.addClass("event_add_selected");
}