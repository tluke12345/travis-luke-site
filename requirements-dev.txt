Django==3.1.6
asgiref==3.3.1
pytz==2021.1
sqlparse==0.4.1
django-environ==0.4.5
requests==2.25.1
# XlsxWriter==1.3.7
wagtail>=2.15,<2.16
boto3==1.20.0
django-storages==1.12.3