from functools import wraps

from django.core.exceptions import PermissionDenied
from django.shortcuts import render

from apps.event.models import Reservation

def validate_reservation(view_func):
    """
    Decorator for views that need to validate reservation and reservation token
    """
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        pk = kwargs.get('pk')
        token = request.GET.get('token')
        reservation = Reservation.public.get_by_pk(pk, token)
        if not reservation:
            raise PermissionDenied()
        
        request.reservation = reservation
        return view_func(request, *args, **kwargs)
    return _wrapped_view

def validate_reservation_or_redirect(view_func):
    @wraps(view_func)
    def _wrapped_view(request, *args, **kwargs):
        pk = kwargs.get('pk')
        token = request.GET.get('token')
        reservation = Reservation.public.get_by_pk(pk, token)
        if not reservation:
            raise PermissionDenied()

        if reservation.event.is_past:
            return render(request, 'website/reservation_is_past.html')
        
        request.reservation = reservation
        return view_func(request, *args, **kwargs)
    return _wrapped_view