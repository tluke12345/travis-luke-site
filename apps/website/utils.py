from datetime import datetime

from django.utils import timezone


def day_is_past(year, month, day):
    if not day:
        return False

    today = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
    date = timezone.make_aware(datetime(year, month, day, 0, 0, 0, 0))
    return today > date
