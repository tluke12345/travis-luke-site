from django import forms
from django.utils.translation import gettext_lazy as _

class ReservationContactform(forms.Form):

    message = forms.CharField(
        max_length=500,
        required=True,
        widget=forms.Textarea(attrs={'cols': 30, 'rows': 5}),
        label=_('Message')
    )