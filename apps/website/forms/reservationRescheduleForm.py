from django import forms
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from apps.event.models import Event


class ReservationRescheduleForm(forms.Form):

    event = forms.ChoiceField(
        label=_('Lesson'),
        choices=[]
    )

    def __init__(self, reservation, *args, **kwargs):
        super(ReservationRescheduleForm, self).__init__(*args, **kwargs)
        events = Event.visible.open_for_reschedule()
        choices = []
        for event in events:
            if reservation.event == event:
                continue
            dt = timezone.localtime(event.datetime)
            choices.append((event.pk, f'{dt: %Y/%m/%d %H:%M}'))
        self.fields['event'].choices = choices
