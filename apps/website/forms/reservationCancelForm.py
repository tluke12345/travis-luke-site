from django import forms
from django.utils.translation import gettext_lazy as _


class ReservationCancelForm(forms.Form):
    
    message = forms.CharField(
        max_length=512,
        required=False,
        widget=forms.Textarea(attrs={'cols': 30, 'rows': 5}),
        label=_('Reason for canceling (optional)')
    )