from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from apps.event.models import Reservation
from apps.event.models import EventType


class ReservationForm(forms.ModelForm):

    event_preference = forms.ModelChoiceField(
        label=_('Type'),
        required=True,
        empty_label=None,
        queryset=EventType.objects.all(),
    )

    class Meta:
        model = Reservation
        fields = [
            'event_preference',
            'name',
            'email',
            'wants_reminder',
            'note',
        ]
        widgets = {
            'note': forms.Textarea(attrs={'cols': 30, 'rows': 5}),
        }
        labels = {
            'name': _('name'),
            'email': _('email'),
            'wants_reminder': _('Please send me a reminder before the lesson'),
            'note': _('message'),
        }

    def __init__(self, *args, **kwargs):
        super(ReservationForm, self).__init__(*args, **kwargs)

        # customer reservations require name and email
        self.fields['name'].required = True
        self.fields['email'].required = True

        if self.instance and hasattr(self.instance, 'event'):
            self.event = self.instance.event
        else:
            self.event = kwargs.get('initial', {}).get('event')

        # No event!
        if not self.event:
            del self.fields['event_preference']

        # Event Type already decided
        elif self.event.event_type or self.event.requested_event_type:
            del self.fields['event_preference']

        # Event Type not decided. Limit type if applicable
        elif self.event.allowed_types.count() > 0:
            self.fields['event_preference'].queryset = self.event.allowed_types.all()

    def clean_email(self):
        canidate_email = self.cleaned_data["email"]
        is_valid = Reservation.objects.is_valid_email_for_event(
            self.event, canidate_email)

        if not is_valid:
            raise ValidationError(_('This Email is Not Valid'))

        return canidate_email
