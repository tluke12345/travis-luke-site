from django.urls import path
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from .views import calendar
from .views import contact
from .views import reservation

urlpatterns = [

    # ROOT -> This just redirects to landing page
    path(
        '',
        RedirectView.as_view(
            pattern_name="english.calendar.current",
            permanent=False),
        name="website.root"
    ),

    # LANDING
    path(
        'faq/',
        TemplateView.as_view(template_name="website/faq.html"),
        name="website.faq"
    ),
    path(
        'intro/',
        TemplateView.as_view(template_name="website/intro.html"),
        name="website.intro"
    ),

    # English services landing page
    # use '#reservation' to scroll to reservation calendar
    path(
        'english/',
        TemplateView.as_view(template_name="website/english.html"),
        name="website.english"
    ),

    # optional 'year:int' and 'month:int' GET params to set calendar month
    # # FRAGMENT - current months calendar
    # path(
    #     'english/calendar/',
    #     calendar.current,
    #     name="website.calendar.current"
    # ),
    # # FRAGMENT - calendar for specified year and day
    # path(
    #     'english/calendar/<int:year>/<int:day>/',
    #     calendar.month,
    #     name="website.calendar.month"
    # ),

    # Web services landing page
    path(
        'web/',
        TemplateView.as_view(template_name="website/web.html"),
        name="website.web"
    ),

    # CALENDAR
    path(
        'calendar/',
        calendar.index,
        name="english_calendar"
    ),
    path(
        'calendar/current/',
        calendar.calendar_current,
        name="english.calendar.current"
    ),
    path(
        'calendar/<int:year>/<int:month>/',
        calendar.month,
        name="english_calendar_month"
    ),
    path(
        'calendar/<int:year>/<int:month>/<int:day>/',
        calendar.day,
        name="english_calendar_day"
    ),

    # RESERVATIONS
    path(
        'reservation/add/',
        reservation.add,
        name="english_reservation_add"
    ),
    path(
        'reservation/<int:pk>/',  # ?token=<reservation.token>
        reservation.index,
        name="english_reservation"
    ),
    path(
        'reservation/<int:pk>/cancel',  # ?token=<reservation.token>
        reservation.cancel,
        name="english_reservation_cancel"
    ),
    path(
        'reservation/reschedule/<int:pk>',   # ?token=<token>
        reservation.reschedule,
        name="english_reservation_reschedule"
    ),

    # CONTACT PAGE
    path(
        'contact/',
        contact.public,
        name="english_contact"
    ),
    path(
        'contact/complete/',
        contact.complete,
        name="english_contact_complete"
    ),
    path(
        'contact/<int:pk>/',    # ?token=<reservation.token>
        contact.reservation,
        name="english_contact_reservation"
    ),
    path(
        'contact/<int:pk>/complete/',    # ?token=<reservation.token>
        contact.reservation_complete,
        name="english_contact_reservation_complete"
    ),
]
