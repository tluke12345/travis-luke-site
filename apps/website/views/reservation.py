from django.core.exceptions import PermissionDenied
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import reverse
from django.utils.translation import gettext as _


from apps.event.models import Event
from apps.event.models import Reservation

from apps.website.decorators import validate_reservation_or_redirect
from apps.website.forms.reservationForm import ReservationForm
from apps.website.forms.reservationCancelForm import ReservationCancelForm
from apps.website.forms.reservationRescheduleForm import ReservationRescheduleForm


def add(request):
    template_name = 'website/reservation_add.html'

    # get event
    event = get_object_or_404(Event, pk=request.GET.get('event'))
    if not event.allow_public_reservation:
        raise Http404(_('Reservations closed'))

    if request.method == 'GET':
        form = ReservationForm(initial={'event': event})

    if request.method == 'POST':
        form = ReservationForm(request.POST, initial={'event': event})
        if form.is_valid():
            reservation = form.save(commit=False)
            reservation.event = event
            Reservation.public.save_new_reservaton(reservation)
            return redirect(reservation.url())

    context = {
        'event': event,
        'form': form,
    }
    return render(request, template_name, context)


@validate_reservation_or_redirect
def index(request, pk):
    template_name = 'website/reservation.html'

    reservation = request.reservation
    context = {
        'event': reservation.event,
        'reservation': reservation,
        'cancel_url': reservation.cancel_url,
        'reschedule_url': reservation.reschedule_url,
    }
    return render(request, template_name, context)


@validate_reservation_or_redirect
def cancel(request, pk):
    template_name = 'website/reservation_cancel.html'

    if request.method == 'GET':
        form = ReservationCancelForm()

    elif request.method == 'POST':
        form = ReservationCancelForm(request.POST)
        if 'cancel_button' in request.POST and form.is_valid():
            msg = form.cleaned_data.get('message')
            Reservation.public.cancel(request.reservation, msg)
            return redirect(request.reservation.url())

    context = {
        'form': form,
    }
    return render(request, template_name, context)


@validate_reservation_or_redirect
def reschedule(request, pk):
    template_name = 'website/reservation_reschedule.html'

    reservation = request.reservation
    if request.method == 'GET':
        form = ReservationRescheduleForm(reservation)

    elif request.method == 'POST':
        form = ReservationRescheduleForm(reservation, request.POST)
        if form.is_valid():
            event_pk = form.cleaned_data.get('event')
            event = get_object_or_404(Event, pk=event_pk)
            Reservation.public.move_to_event(reservation, event)
            return redirect(reservation.url())

    context = {
        'form': form,
    }

    return render(request, template_name, context)
