
from django.http import Http404
from django.shortcuts import redirect
from django.shortcuts import render
from django.utils import timezone
from django.utils.translation import gettext as _

from apps.calendar.utils import clamped_is_valid_date
from apps.calendar.utils import clamped_next_url
from apps.calendar.utils import clamped_prev_url
from apps.event.models import Event

from apps.website.utils import day_is_past


def index(request):
    # next event visible on calendar
    dt = Event.visible.next_event_date() or timezone.localtime(timezone.now())
    return redirect('english_calendar_month', year=dt.year, month=dt.month)


def calendar_current(request):
    dt = Event.visible.next_event_date() or timezone.localtime(timezone.now())
    year = dt.year
    month = dt.month

    template_name = 'website/calendar.html'
    view_name = 'english_calendar_month'
    context = {
        'year': year,
        'month': month,
        'calendar_items': Event.visible.calendar_items_for_month(year, month),
        'events_by_day': Event.visible.upcoming_events_by_day(year, month),
        'calendar_options': {
            'buttons': True,
            'buttons_ignore': ('past', 'empty',),
            'next_url': clamped_next_url(year, month, view_name),
            'prev_url': clamped_prev_url(year, month, view_name),
        }
    }
    return render(request, template_name, context)


def month(request, year, month):
    return day(request, year, month, None)


def day(request, year, month, day):
    template_name = 'website/calendar.html'

    # Ensure the user input isn't too wild
    if not clamped_is_valid_date(year, month):
        raise Http404(_('Invalid Date'))

    # Only show day if event exist and not in the past
    if day:
        event_count = Event.visible.for_day(year, month, day).count()
        if event_count == 0 or day_is_past(year, month, day):
            return redirect('english_calendar_month', year=year, month=month)

    view_name = 'english_calendar_month'
    context = {
        'year': year,
        'month': month,
        'day': day,
        'calendar_items': Event.visible.calendar_items_for_month(year, month),
        'events_by_day': Event.visible.upcoming_events_by_day(year, month),
        'calendar_options': {
            'buttons': True,
            # other options 'future', and 'today'
            'buttons_ignore': ('past', 'empty',),
            'next_url': clamped_next_url(year, month, view_name),
            'prev_url': clamped_prev_url(year, month, view_name),
            # 'title': 'a title here',
            # 'subtitle': 'a subtitle',
        }
    }
    return render(request, template_name, context)
