import logging

from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import reverse

from apps.website.decorators import validate_reservation
from apps.website.forms.contactForm import Contactform
from apps.website.forms.reservationContactForm import ReservationContactform

from apps.event.models import Reservation
from apps.message.models import Message


logger = logging.getLogger('spam')


def public(request):
    """
    Public Contact View
    """
    template_name = 'website/contact.html'

    if request.method == 'GET':
        form = Contactform()

    if request.method == 'POST':
        form = Contactform(request.POST)

        # robo-spam check -> check the hidden (display: none;) field is nothing
        if request.POST.get('human'):
            logger.warning(f'spam:hidden_field:{request.POST.get("email")}')
            print(f'spam: {request.POST.get("email")}')
            return redirect('english_calendar')

        # Not robo-spam, continue
        if 'send_button' in request.POST and form.is_valid():
            msg = form.cleaned_data.get('message')
            email = form.cleaned_data.get('email')
            Message.objects.create_public_message(email, msg)

            url = reverse('english_contact_complete')
            return redirect(url)

    context = {
        'form': form
    }
    return render(request, template_name, context)


def complete(request):
    """
    Public Contact Complete View
    """
    template_name = 'website/contact_complete.html'
    context = {}
    return render(request, 'website/contact_complete.html', context)


@validate_reservation
def reservation(request, pk):
    """
    Semi-private Contact View (connected to reservation)
    """
    template_name = 'website/contact.html'

    reservation = request.reservation
    if request.method == 'GET':
        form = ReservationContactform()

    if request.method == 'POST':
        form = ReservationContactform(request.POST)
        if 'send_button' in request.POST and form.is_valid():
            msg = form.cleaned_data.get('message')
            Message.objects.create_reservation_message(reservation, msg)

            url = reverse('english_contact_reservation_complete', args=[pk])
            url += f'?token={reservation.token}'
            return redirect(url)

    context = {
        'form': form
    }
    return render(request, template_name, context)


@validate_reservation
def reservation_complete(request, pk):
    """
    semi-private Contact Complete View
    """
    template_name = 'website/contact_complete.html'
    context = {
        'reservation': request.reservation,
    }
    return render(request, 'website/contact_complete.html', context)
