from django.utils import timezone

# from apps.email.backends.mailgun import send
from apps.email.backends.sendinblue import send
from apps.email.utils import get_email_address
from apps.email.utils import render_template


def send_reservation_confirmation(reservation, test_mode=None):
    context = {'reservation': reservation}
    html = render_template(
        'email/email_reservation_confirmation.html', context)
    text = render_template('email/email_reservation_confirmation.txt', context)

    dt = timezone.localtime(reservation.event.datetime)
    date = f'{dt.month}/{dt.day}'
    receipt = send(
        sender=get_email_address('SUPPORT_EMAIL'),
        sender_name=None,
        recipient=reservation.email,
        subject=f"【予約】{date} - 英会話レッスン",
        html=html,
        text=text,
        commit=False,
        test_mode=test_mode,
    )
    receipt.email_type = 'reservation.confirmation'
    receipt.related_type = "Reservation"
    receipt.related_id = reservation.pk
    receipt.save()

    is_success = receipt.status_code == 201
    return is_success


def send_reservation_reminder(reservation, test_mode=None):
    context = {'reservation': reservation}
    html = render_template('email/email_reservation_reminder.html', context)
    text = render_template('email/email_reservation_reminder.txt', context)

    dt = timezone.localtime(reservation.event.datetime)
    receipt = send(
        sender=get_email_address('SUPPORT_EMAIL'),
        sender_name=None,
        recipient=reservation.email,
        subject=f"【リマインダー】明日の英会話レッスン",
        html=html,
        text=text,
        commit=False,
        test_mode=test_mode,
    )
    receipt.email_type = 'reservation.reminder'
    receipt.related_type = "Reservation"
    receipt.related_id = reservation.pk
    receipt.save()

    is_success = receipt.status_code == 201
    return is_success
