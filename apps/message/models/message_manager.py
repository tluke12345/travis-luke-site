from django.db import models

from apps.eventManager.email import send_message_received
from .message_queryset import MessageQuerySet


TEST_SENDER = 'test@test.com'


class MessageManager(models.Manager):
    def get_queryset(self):
        return MessageQuerySet(self.model, using=self._db)

    def delete(self, message):
        message.delete()

    def ignore_sender(self, message):
        # TODO ignore
        print(f'TODO: ignore -> {message.sender}')

    def get_by_pk(self, pk):
        if not pk:
            return None

        message = self.get_queryset().filter(pk=pk).first()
        return message

    # --------------------------------------------------------------------------
    # Creation
    # --------------------------------------------------------------------------
    def create_public_message(self, email, msg):
        message = self.get_queryset().create(
            sender=email,
            content=msg,
        )
        send_message_received(message)
        return message

    def create_reservation_message(self, reservation, msg):
        message = self.get_queryset().create(
            sender=reservation.email,
            sender_type='reservation',
            sender_id=reservation.pk,
            content=msg
        )
        send_message_received(message)
        return message

    # --------------------------------------------------------------------------
    # Testing
    # --------------------------------------------------------------------------
    def example(self):
        return self.get_queryset().first()

    def create_test_messages(self, count):
        msg = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu pellentesque massa. Cras consequat est et mauris posuere dapibus. Ut tincidunt non massa nec luctus. Vestibulum sodales odio erat, fermentum bibendum enim imperdiet ut. Aliquam vitae augue orci. Praesent vitae risus vel dui posuere varius at eu neque. Ut malesuada ultricies nisl, id consectetur dui maximus eget. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut posuere hendrerit egestas. '
        for i in range(count):
            self.get_queryset().create(
                sender=TEST_SENDER,
                content=f'{i}: {msg}'
            )
        return count

    def delete_test_messages(self):
        messages = self.get_queryset().filter(sender=TEST_SENDER)
        count = messages.count()
        for message in messages:
            self.delete(message)
        return count
