from django.apps import apps
from django.db import models
from django.shortcuts import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from .message_manager import MessageManager


class Message(models.Model):
    timestamp = models.DateTimeField(default=timezone.now)
    sender = models.CharField(max_length=50)
    sender_type = models.CharField(max_length=50, blank=True)
    sender_id = models.IntegerField(null=True, blank=True)
    content = models.TextField(max_length=500, blank=True)

    objects = MessageManager()

    class Meta:
        ordering = ('-timestamp',)

    def __str__(self):
        dt = timezone.localtime(self.timestamp)
        time = f'{dt: %Y/%m/%d %H:%M}'
        return f'{time}: {self.sender or "no sender"}'

    def get_absolute_url(self):
        return reverse('em.messages.index')
    
    @property
    def reservation(self):
        if self.sender_type == 'reservation' and self.related_id:
            Reservation = apps.get_model(
                app_label='event', model_name="Reservation")
            return Reservation.objects.get_by_pk(self.sender_id)
    
    @property
    def is_trusted_sender(self):
        # TODO: check if previous customer?
        return False