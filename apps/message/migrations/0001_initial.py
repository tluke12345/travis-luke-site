# Generated by Django 3.1.6 on 2021-04-21 07:17

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('timestamp', models.DateTimeField(default=django.utils.timezone.now)),
                ('sender', models.CharField(max_length=50)),
                ('sender_type', models.CharField(blank=True, max_length=50)),
                ('sender_id', models.IntegerField(blank=True, null=True)),
                ('content', models.TextField(blank=True, max_length=500)),
            ],
            options={
                'ordering': ('timestamp',),
            },
        ),
    ]
