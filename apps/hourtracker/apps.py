from django.apps import AppConfig


class HourTrackerConfig(AppConfig):
    name = 'apps.hourtracker'
