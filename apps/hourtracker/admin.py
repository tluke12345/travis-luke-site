from django.contrib import admin

from apps.hourtracker.models import Category
from apps.hourtracker.models import Entry

# Category Actions
# ------------------------------------------------------------------------------


def recalculate_balance(modelAdmin, request, queryset):
    for category in queryset:
        category.recalculate_balance()


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'balance',
        'entry_count'
    ]
    actions = [
        recalculate_balance,
    ]

    def entry_count(self, obj):
        return obj.entries.count()


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = [
        'datetime',
        'category',
        'hours'
    ]
