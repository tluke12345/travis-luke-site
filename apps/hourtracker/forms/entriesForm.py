from django import forms

from apps.hourtracker.models import Entry


class EntryForm(forms.ModelForm):

    class Meta:
        model = Entry
        fields = [
            'category',
            'datetime',
            'hours',
            'quota',
            'note',
        ]
        widgets = {
            # 'category': forms.HiddenInput(),
            'datetime': forms.HiddenInput(),
            'note': forms.Textarea(attrs={'cols': 30, 'rows': 5}),
        }
