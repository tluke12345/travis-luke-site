from django.urls import path

from apps.hourtracker.views import entryView as entry
from apps.hourtracker.views import categoriesView as categories
from apps.hourtracker.views import entriesView as entries

urlpatterns = [

    # Entry
    path(
        '',
        entry.index,
        name="hours"
    ),

    # Category
    path(
        'categories/<name>/',
        categories.item,
        name="hours.categories.item"
    ),

    # Entry
    path(
        'entries/new/<category_name>',
        entries.new,
        name="hours.entries.new"
    ),
]
