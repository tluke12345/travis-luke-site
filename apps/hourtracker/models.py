from django.db import models


class CategoryQuerySet(models.QuerySet):
    def active(self):
        return self


class CagegoryManager(models.Manager):
    def get_queryset(self):
        return CategoryQuerySet(self.model, using=self._db)

    def first_category(self):
        return self.get_queryset().first()

    def all_categories_annotated(self, active_category):
        all_categories = self.get_queryset()
        for category in all_categories:
            if active_category and category == active_category:
                category.active = True
        return all_categories


class Category(models.Model):
    name = models.CharField(db_index=True, max_length=256)
    default_quota = models.FloatField(default=10.0)
    balance = models.FloatField(default=0.0)
    objects = CagegoryManager()

    def __str__(self) -> str:
        return self.name

    class Meta:
        verbose_name_plural = 'categories'

    def recalculate_balance(self):
        b = 0.0
        for entry in self.entries.all():
            b += entry.delta

        self.balance = b
        self.save()


class EntryQuerySet(models.QuerySet):
    pass


class EntryManager(models.Manager):
    def save(self, entry):
        entry.save()
        entry.category.recalculate_balance()

    def get_queryset(self):
        return EntryQuerySet(self.model, using=self._db)

    def entries_for_category_annotated(self, category):
        if not category:
            return None

        b = 0.0
        entries = []
        for entry in category.entries.order_by('datetime'):
            b += entry.delta
            entry.balance = b
            entries.insert(0, entry)
        return entries


class Entry(models.Model):
    category = models.ForeignKey(
        Category,
        related_name='entries',
        on_delete=models.CASCADE,
    )
    datetime = models.DateTimeField()
    hours = models.FloatField(default=0.0)
    quota = models.FloatField(null=True)
    note = models.TextField(blank=True)

    objects = EntryManager()

    class Meta:
        ordering = ('-datetime',)
        verbose_name_plural = 'entries'

    @property
    def delta(self):
        if self.quota:
            return self.hours - self.quota
        else:
            return self.hours
