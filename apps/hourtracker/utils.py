import datetime
from django.utils import timezone


def weekdt_for_today():
    return weekdt_for_datetime(datetime.datetime.today())


def weekdt_list_for_today():
    today = datetime.datetime.today()
    start = today - datetime.timedelta(weeks=13)
    end = today + datetime.timedelta(weeks=2)
    return weekdt_list(start=start, end=end)


def weekdt_for_datetime(dt: datetime.datetime):
    '''determine the 'weekdt'
    Each week starts on sunday and ends on saturday
    Each week is represented by the weekdt
    The weekdt is datetime where date -> Sunday, time -> 00:00:00
    weekdt is localized using django.utils.timezone
    '''

    weekdt = dt.replace(hour=0, minute=0, second=0, microsecond=0)
    if weekdt.weekday() != 6:
        offset = weekdt.weekday() + 1
        weekdt += datetime.timedelta(days=-offset)
    weekdt = timezone.make_aware(weekdt)
    return weekdt


def weekdt_list(start: datetime.datetime, end: datetime.datetime):
    '''list of all weekdt in specified range'''

    result = []
    while start < end:
        result.append(weekdt_for_datetime(start))
        start += datetime.timedelta(weeks=1)

    return result
