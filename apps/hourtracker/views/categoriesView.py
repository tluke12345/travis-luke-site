from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, get_object_or_404

from apps.hourtracker.decorators import is_hourtracker_user
from apps.hourtracker.models import Category, Entry


@user_passes_test(is_hourtracker_user)
def item(request, name):
    template_name = 'hourtracker/categories/item.html'

    active_category = Category.objects.filter(name=name).first()
    all_categories = Category.objects.all_categories_annotated(active_category)
    entries = Entry.objects.entries_for_category_annotated(active_category)

    context = {
        'all_categories': all_categories,
        'active_category': active_category,
        'entries': entries,
    }
    return render(request, template_name, context)