import datetime
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import redirect, render, get_object_or_404

from apps.hourtracker.decorators import is_hourtracker_user
from apps.hourtracker.models import Category, Entry
from apps.hourtracker.forms.entriesForm import EntryForm
from apps.hourtracker.utils import weekdt_for_today, weekdt_list_for_today


@user_passes_test(is_hourtracker_user)
def new(request, category_name):
    template_name = "hourtracker/entries/new.html"
    category = get_object_or_404(Category, name=category_name)
    weekdt = weekdt_for_today()
    weekdt_options = [{
        'dt': str(dt),
        'iso': dt.isoformat(),
        'text': f'{dt: %m/%d} ~ {(dt + datetime.timedelta(days=6)): %m/%d}',
        'selected': dt == weekdt,
    } for dt in weekdt_list_for_today()]
    initial = {
        'datetime': weekdt,
        'category': category,
        'quota': category.default_quota,
        'hours': None,
    }

    if request.method == 'GET':
        form = EntryForm(initial=initial)
    elif request.method == 'POST':
        form = EntryForm(request.POST, initial=initial)
        if form.is_valid():
            entry = form.save(commit=False)
            Entry.objects.save(entry)
            return redirect('hours.categories.item', name=category_name)

    context = {
        'category': category,
        'form': form,
        'weekdt': weekdt,
        'weekdt_list': weekdt_options,
    }
    return render(request, template_name, context)
