from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import redirect

from apps.hourtracker.decorators import is_hourtracker_user
from apps.hourtracker.models import Category


@user_passes_test(is_hourtracker_user)
def index(request):
    category = Category.objects.first_category()
    return redirect('hours.categories.item', category.name)
