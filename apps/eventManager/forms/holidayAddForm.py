from django import forms

from apps.calendar.models import NamedDay


class HolidayAddForm(forms.ModelForm):

    day = forms.IntegerField(
        required=True
    )

    class Meta:
        model = NamedDay
        fields = [
            # 'date',
            'day',
            'name_en',
            'name_ja',
            'disable_calendar',
        ]
