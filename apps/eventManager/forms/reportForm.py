import datetime
from django import forms

from apps.forms.widgets import TagSelect

YEARS = []
for year in range(2021, datetime.datetime.now().year + 1):
    YEARS.append((year, year))
MONTHS = []
for month in range(1, 13):
    MONTHS.append((month, month))


class ReportForm(forms.Form):

    report_type = forms.ChoiceField(
        label="Type",
        choices=[('events', 'Events'), ('income', 'Income')],
        widget=TagSelect()
    )
    year = forms.ChoiceField(
        label="Year",
        choices=YEARS,
        # widget=TagSelect()
    )
    month = forms.ChoiceField(
        label="Month",
        choices=MONTHS,
        # widget=TagSelect()
    )
