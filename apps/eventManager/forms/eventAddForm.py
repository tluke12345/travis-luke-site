from calendar import monthrange
import datetime

from django import forms
from django.forms import widgets

from apps.forms.fields import MinutesDurationField
from apps.forms.widgets import TagSelectMultiple
from apps.forms.widgets import TagSelect
from apps.event.models import Event

TIMES = [
    ('10:30', '10:30')
]
for h in range(12, 17):
    TIMES.append((f'{h}:00', f'{h}:00'))


class EventAddForm(forms.ModelForm):

    def __init__(self, year, month, *args, **kwargs):
        super(EventAddForm, self).__init__(*args, **kwargs)
        dates = range(1, monthrange(year=year, month=month)[1] + 1)
        self.fields['date'].choices = [(str(d), str(d)) for d in dates]

    # TODO Hide this widget somehow
    date = forms.MultipleChoiceField(
        label="date(s)",
        choices=[],
    )
    time = forms.MultipleChoiceField(
        label="time(s)",
        choices=TIMES,
        widget=TagSelectMultiple(),
    )
    close_days_before = forms.IntegerField(
        label="Close reservations # days before Event",
        initial=1
    )
    close_time = forms.TimeField(
        label="Close reservations at time",
        initial=datetime.time(17, 0, 0, 0),
        widget=forms.TimeInput(attrs={'type': 'time'}),
    )
    duration = MinutesDurationField(
        label="Duration (Minutes)",
        initial=datetime.timedelta(minutes=60)
    )

    class Meta:
        model = Event
        fields = [
            'date',
            'time',
            'duration',
            'close_days_before',
            'close_time',
            'allowed_types',
            'event_type',
            'location',
            'custom_price',
            'custom_reservation_limit',
            'is_public',
        ]
        widgets = {
            'allowed_types': TagSelectMultiple,
            'event_type': TagSelect,
            'location': TagSelect,
            # 'is_public': TagToggle,
        }
