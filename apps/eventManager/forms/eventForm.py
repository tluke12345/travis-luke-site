import datetime
from django import forms

from apps.forms.fields import MinutesDurationField
from apps.forms.widgets import TagSelectMultiple
from apps.event.models import Event


class EventForm(forms.ModelForm):

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        if self.instance:
            # Current Price
            p = f'{self.instance.price} ({self.instance.event_type})'
            self.fields['custom_price'].widget.attrs['placeholder'] = p
            # Current Reservation Limit
            r = f'{self.instance.reservation_limit} ({self.instance.event_type})'
            self.fields['custom_reservation_limit'].widget.attrs['placeholder'] = r

    datetime = forms.SplitDateTimeField(
        widget=forms.SplitDateTimeWidget(
            date_attrs={'type': 'date'},
            time_attrs={'type': 'time'},
        )
    )
    close_at = forms.SplitDateTimeField(
        widget=forms.SplitDateTimeWidget(
            date_attrs={'type': 'date'},
            time_attrs={'type': 'time'},
        )
    )
    duration = MinutesDurationField(
        label="Duration (Minutes)"
    )

    class Meta:
        model = Event
        fields = [
            'datetime',
            'duration',
            'close_at',

            'allowed_types',
            'event_type',
            'location',
            'custom_price',
            'custom_reservation_limit',

            'notes',
        ]
        widgets = {
            'notes': forms.Textarea(attrs={'cols': 30, 'rows': 5}),
            'allowed_types': TagSelectMultiple,
        }
