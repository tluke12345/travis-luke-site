from django import forms

from apps.event.models import Location


class LocationForm(forms.ModelForm):

    class Meta:
        model = Location
        fields = [
            'name_en',
            'name_ja',
        ]