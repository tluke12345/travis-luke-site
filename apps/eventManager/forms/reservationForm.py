from django import forms
from apps.event.models import Reservation


class ReservationForm(forms.ModelForm):

    class Meta:
        model = Reservation
        fields = [
            'problem_message',
            'event',
            'event_preference',
            'name',
            'email',
            'wants_reminder',
            'note',
        ]
        widgets = {
            'note': forms.Textarea(attrs={'cols': 30, 'rows': 5}),
        }
