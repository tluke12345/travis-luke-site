from django import forms
from apps.event.models import Reservation

class ReservationAddForm(forms.ModelForm):

    class Meta:
        model = Reservation
        fields = [
            'event_preference',
            'name',
            'email',
            'wants_reminder',
            'note',
        ]

