from django.urls import path
from apps.eventManager.views import customersView as customers
from apps.eventManager.views import emailsView as emails
from apps.eventManager.views import eventsView as events
from apps.eventManager.views import locationsView as locations
from apps.eventManager.views import messagesView as messages
from apps.eventManager.views import namedDaysView as namedDays
from apps.eventManager.views import reportsView as reports
from apps.eventManager.views import reservationsView as reservations

urlpatterns = [

    # EVENTS
    # --------------------------------------------------------------------------
    path(
        'events/calendar/',
        events.calendar,
        name="em.events.calendar"
    ),
    path(
        'events/calendar/current/',
        events.calendar_current,
        name="em.events.calendar.current"
    ),
    path(
        'events/calendar/<int:year>/<int:month>/',
        events.calendar_month,
        name="em.events.calendar.month"
    ),
    path(
        'events/new/',
        events.new,
        name="em.events.new"
    ),
    path(
        'events/new/<int:year>/<int:month>/',
        events.new_month,
        name="em.events.new.month"
    ),
    path(
        'events/<int:pk>/',
        events.item,
        name="em.events.item"
    ),
    path(
        'events/<int:pk>/edit/',
        events.edit,
        name="em.events.edit"
    ),

    # RESERVATION
    # --------------------------------------------------------------------------
    path(
        'reservations/new/',  # ?event=<event_pk>
        reservations.new,
        name="em.reservations.new"
    ),
    path(
        'reservations/<int:pk>/',
        reservations.item,
        name="em.reservations.item"
    ),
    path(
        'reservations/<int:pk>/edit/',
        reservations.edit,
        name="em.reservations.edit"
    ),

    # LOCATIONS
    # --------------------------------------------------------------------------
    path(
        'locations/',  # paginated
        locations.index,
        name="em.locations.index"
    ),
    path(
        'locations/new/',
        locations.new,
        name="em.locations.new"
    ),
    path(
        'locations/search/',
        locations.search,
        name="em.locations.search"
    ),
    path(
        'locations/<int:pk>/',
        locations.item,
        name="em.locations.item"
    ),

    # MESSAGES
    # --------------------------------------------------------------------------
    path(
        'messages/',
        messages.index,
        name="em.messages.index"
    ),
    path(
        'messages/<int:pk>/',
        messages.item,
        name="em.messages.item"
    ),

    # EMAILS
    # --------------------------------------------------------------------------
    path(
        'emails/',
        emails.index,
        name="em.emails.index"
    ),
    path(
        'emails/<int:pk>/',
        emails.item,
        name="em.emails.item"
    ),
    path(
        'emails/preview/',
        emails.preview,
        name="em.emails.preview"
    ),
    path(
        'emails/preview/<name>/',
        emails.preview_html,
        name="em.emails.preview.html"
    ),

    # NAMED DAYS (Holidays)
    # --------------------------------------------------------------------------
    path(
        'nameddays/new/',
        namedDays.new,
        name="em.namedDays.new"
    ),
    path(
        'nameddays/new/<int:year>/<int:month>/',
        namedDays.new_month,
        name="em.namedDays.new.month"
    ),

    # CUSTOMERS
    # --------------------------------------------------------------------------
    path(
        'customers/',
        customers.index,
        name="em.customers.index"
    ),
    path(
        'customers/search/',
        customers.search,
        name="em.customers.search"
    ),

    # REPORTS
    # --------------------------------------------------------------------------
    path(
        'reports/',
        reports.index,
        name="em.reports.index"
    ),
]
