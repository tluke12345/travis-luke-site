from django.apps import AppConfig


class EventManagerConfig(AppConfig):
    name = 'apps.eventManager'
