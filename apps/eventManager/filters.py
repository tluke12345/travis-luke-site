import django_filters
from django_filters.filters import OrderingFilter

from apps.email.models import EmailReceipt
from apps.message.models import Message


class EmailReceiptFilter(django_filters.FilterSet):

    order = OrderingFilter(
        fields=(
            ('timestamp', 'timestamp'),
            ('sender', 'sender'),
            ('recipient', 'recipient'),
            ('status_code', 'status_code'),
            ('email_type', 'email_type'),
            ('related_type', 'related_type'),
        ),
    )

    class Meta:
        model = EmailReceipt
        fields = {
            'sender': ['icontains'],
            'recipient': ['icontains'],
            'email_type': ['icontains'],
            'timestamp': ['day', 'month', 'year'],
        }


class MessageFilter(django_filters.FilterSet):

    order = OrderingFilter(
        fields=(
            ('timestamp', 'timestamp'),
            ('sender', 'sender'),
            ('sender_type', 'sender_type'),
        ),
    )

    class Meta:
        model = Message
        fields = {
            'sender': ['icontains'],
            'sender_type': ['icontains'],
            'content': ['icontains'],
            'timestamp': ['day', 'month', 'year'],
        }