# from django.utils import timezone

from apps.core.utils import time_until

# from apps.email.backends.mailgun import send
from apps.email.backends.sendinblue import send
from apps.email.utils import get_email_address
from apps.email.utils import render_template
from apps.email.utils import text_from_html


SENDER_NAME = "Event Manager"

def send_reservation_received(reservation):
    context = {'reservation': reservation}
    html = render_template('email/em_email_reservation_received.html', context)
    text = text_from_html(html)

    receipt = send(
        sender=get_email_address('SUPPORT_EMAIL'),
        sender_name=SENDER_NAME,
        recipient=get_email_address('ADMIN_EMAIL'),
        subject=f'New Reservation',
        html=html,
        text=text,
        commit=False,
        reply_to=reservation.email,
    )
    receipt.email_type = 'manager.reservation.received'
    receipt.related_type = "Reservation"
    receipt.related_id = reservation.pk
    receipt.save()

    is_success = receipt.status_code == 201
    return is_success


def send_reservation_canceled(reservation):
    context = {'reservation': reservation}
    html = render_template('email/em_email_reservation_canceled.html', context)
    text = text_from_html(html)

    receipt = send(
        sender=get_email_address('SUPPORT_EMAIL'),
        sender_name=SENDER_NAME,
        recipient=get_email_address('ADMIN_EMAIL'),
        subject='Reservation canceled',
        html=html,
        text=text,
        commit=False,
        reply_to=reservation.email,
    )
    receipt.email_type = 'manager.reservation.canceled'
    receipt.related_type = "Reservation"
    receipt.related_id = reservation.pk
    receipt.save()

    is_success = receipt.status_code == 201
    return is_success


def send_reservation_moved(reservation, old_event):
    context = {
        'reservation': reservation,
        'old_event': old_event,
    }
    html = render_template('email/em_email_reservation_moved.html', context)
    text = text_from_html(html)

    receipt = send(
        sender=get_email_address('SUPPORT_EMAIL'),
        sender_name=SENDER_NAME,
        recipient=get_email_address('ADMIN_EMAIL'),
        subject='Reservation moved',
        html=html,
        text=text,
        commit=False,
        reply_to=reservation.email,
    )
    receipt.email_type = 'manager.reservation.moved'
    receipt.related_type = "Reservation"
    receipt.related_id = reservation.pk
    receipt.save()

    is_success = receipt.status_code == 201
    return is_success


def send_message_received(message):
    context = {'message': message}
    html = render_template('email/em_email_message_received.html', context)
    text = text_from_html(html)

    receipt = send(
        sender=get_email_address('SUPPORT_EMAIL'),
        sender_name=SENDER_NAME,
        recipient=get_email_address('ADMIN_EMAIL'),
        subject='Message received',
        html=html,
        text=text,
        commit=False,
        reply_to=message.sender,
    )
    receipt.email_type = 'manager.message.received'
    receipt.related_type = "Message"
    receipt.related_id = message.pk
    receipt.save()

    is_success = receipt.status_code == 201
    return is_success


def send_reminders_summary(summary, test_mode=None):
    context = {'summary': summary}
    html = render_template('email/em_email_reminder_summary.html', context)
    text = text_from_html(html)

    receipt = send(
        sender=get_email_address('SUPPORT_EMAIL'),
        sender_name=SENDER_NAME,
        recipient=get_email_address('ADMIN_EMAIL'),
        subject='Tomorrows Events',
        html=html,
        text=text,
        commit=False,
        test_mode=test_mode,
    )
    receipt.email_type = "manager.send_reminders.summary"
    receipt.save()

    is_success = receipt.status_code == 201
    return is_success
