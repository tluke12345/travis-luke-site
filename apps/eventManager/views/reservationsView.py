from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import reverse

from apps.core.http import htmx_redirect
from apps.core.http import htmx_refresh
from apps.eventManager.decorators import is_manager
from apps.website.email import send_reservation_reminder

from apps.event.models import Event
from apps.event.models import Reservation
from apps.email.models import EmailReceipt

from apps.eventManager.forms.reservationForm import ReservationForm
from apps.eventManager.forms.reservationAddForm import ReservationAddForm


@user_passes_test(is_manager)
def new(request):
    template_name = 'eventManager/reservations_new.html'
    event = get_object_or_404(Event, pk=request.GET.get('event'))

    if request.method == 'GET':
        form = ReservationAddForm()

    if request.method == 'POST':
        form = ReservationAddForm(request.POST)
        if form.is_valid():
            reservation = form.save(commit=False)
            # form 'initial' arg doesn't work because form has no 'event' field!
            reservation.event = event
            Reservation.objects.save(reservation)
            url = reverse('em.events.item', args=[event.pk])
            return redirect(url)

    context = {
        'form': form,
        'event': event,
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def item(request, pk):
    template_name = 'eventManager/reservations_item.html'
    reservation = get_object_or_404(Reservation, pk=pk)

    if request.method == 'DELETE':
        event_url = reverse('em.events.item', args=[reservation.event.pk])
        Reservation.objects.delete(reservation)
        return htmx_redirect(event_url)

    if request.method == 'POST':
        if request.POST.get('action') == "cancel":
            Reservation.objects.cancel(reservation)
        elif request.POST.get('action') == "uncancel":
            Reservation.objects.uncancel(reservation)
        elif request.POST.get('action') == 'send_reminder':
            send_reservation_reminder(reservation)
        return htmx_refresh()

    email_receipts = EmailReceipt.objects \
        .customer_receipts_for_reservation(reservation)
    context = {
        'reservation': reservation,
        'emails': email_receipts,
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def edit(request, pk):
    template_name = 'eventManager/reservations_edit.html'
    reservation = get_object_or_404(Reservation, pk=pk)

    if request.method == 'GET':
        form = ReservationForm(instance=reservation)

    if request.method == 'POST':
        form = ReservationForm(request.POST, instance=reservation)
        if form.is_valid():
            reservation = form.save(commit=False)
            Reservation.objects.save(reservation)
            url = reverse('em.reservations.item', args=[reservation.pk])
            return redirect(url)

    context = {
        'form': form,
        'reservation': reservation,
        'event': reservation.event,
    }
    return render(request, template_name, context)
