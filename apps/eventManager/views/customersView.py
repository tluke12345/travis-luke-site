from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render

from apps.core.utils import paginate
from apps.eventManager.decorators import is_manager
from apps.eventManager.utils import customer_dictionaries
from apps.event.models import Reservation


@user_passes_test(is_manager)
def index(request):
    template_name = 'eventManager/customers_index.html'
    customers = customer_dictionaries(Reservation.objects.all())
    context = {
        'customers': paginate(customers, 0, 20),
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def search(request):
    template_name = 'eventManager/includes/customers_page.html'

    if request.method == "GET":
        query = request.GET.get('query')
        page = request.GET.get('page')
        reservations = Reservation.objects.matching_query(query)
        customers = customer_dictionaries(reservations)

        context = {
            'customers': paginate(customers, page, 20),
        }
        return render(request, template_name, context)
