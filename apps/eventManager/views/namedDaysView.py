import datetime
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import redirect
from django.shortcuts import render
from django.utils import timezone

from apps.calendar import utils
from apps.calendar.models import NamedDay
from apps.event.models import Event

from apps.eventManager.decorators import is_manager
from apps.eventManager.forms.holidayAddForm import HolidayAddForm


@user_passes_test(is_manager)
def new(request):
    dt = timezone.now()
    return redirect('em.namedDays.new.month', year=dt.year, month=dt.month)


@user_passes_test(is_manager)
def new_month(request, year, month):
    template_name = 'eventManager/namedDays_new_month.html'

    if request.method == 'GET':
        form = HolidayAddForm()

    if request.method == 'POST':
        form = HolidayAddForm(request.POST)
        if form.is_valid():
            day = form.cleaned_data.get('day')
            date = datetime.date(year, month, day)
            namedDay = form.save(commit=False)
            namedDay.date = date
            NamedDay.objects.save(namedDay)

            # redirect to this same view resets the form values
            return redirect('em.namedDays.new.month', year=year, month=month)

    view_name = 'em.namedDays.new.month'
    context = {
        'year': year,
        'month': month,
        'form': form,
        'calendar_items': Event.objects.small_calendar_items_for_month(year, month),
        'calendar_options': {
            'buttons': True,
            'next_url': utils.next_url(year, month, view_name),
            'prev_url': utils.prev_url(year, month, view_name),
        }
    }
    return render(request, template_name, context)
