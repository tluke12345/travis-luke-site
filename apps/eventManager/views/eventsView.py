from django.contrib.auth.decorators import user_passes_test
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import reverse
from django.utils import timezone

from apps.core.http import htmx_redirect
from apps.core.http import htmx_refresh
from apps.calendar import utils

from apps.eventManager.decorators import is_manager
from apps.eventManager.utils import calc_datetimes
from apps.eventManager.utils import calc_close_datetime

from apps.eventManager.forms.eventForm import EventForm
from apps.eventManager.forms.eventAddForm import EventAddForm

from apps.event.models import Event
from apps.event.models import EventType


@user_passes_test(is_manager)
def calendar(request):
    dt = timezone.localtime(timezone.now())
    return redirect('em.events.calendar.month', year=dt.year, month=dt.month)


@user_passes_test(is_manager)
def calendar_current(request):
    dt = timezone.localtime(timezone.now())
    year = dt.year
    month = dt.month

    template_name = 'eventManager/events_calendar_month.html'
    view_name = 'em.events.calendar.month'
    context = {
        'year': year,
        'month': month,
        'calendar_items': Event.objects.calendar_items_for_month(year, month),
        'calendar_options': {
            'next_url': utils.next_url(year, month, view_name),
            'prev_url': utils.prev_url(year, month, view_name),
        }
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def calendar_month(request, year, month):
    template_name = 'eventManager/events_calendar_month.html'

    # Ensure the user input isn't too wild
    if not utils.is_valid_date(year, month):
        raise Http404('invalid date')

    view_name = 'em.events.calendar.month'
    context = {
        'year': year,
        'month': month,
        'calendar_items': Event.objects.calendar_items_for_month(year, month),
        'calendar_options': {
            'next_url': utils.next_url(year, month, view_name),
            'prev_url': utils.prev_url(year, month, view_name),
        }
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def new(request):
    dt = timezone.now()
    return redirect('em.events.new.month', year=dt.year, month=dt.month)


@user_passes_test(is_manager)
def new_month(request, year, month):
    template_name = 'eventManager/events_new_month.html'
    initial = {'allowed_types': EventType.objects.all()}

    if request.method == 'GET':
        form = EventAddForm(year, month, initial=initial)

    if request.method == 'POST':
        form = EventAddForm(year, month, request.POST, initial=initial)
        if form.is_valid():
            datetimes = calc_datetimes(
                year,
                month,
                form.cleaned_data.get('date'),
                form.cleaned_data.get('time')
            )
            close_delta = form.cleaned_data['close_days_before'] * -1
            close_time = form.cleaned_data['close_time']
            allowed_types = form.cleaned_data['allowed_types']
            event = form.save(commit=False)

            for dt in datetimes:
                close_at = calc_close_datetime(
                    dt,
                    close_delta,
                    close_time,
                )
                event.pk = None
                event.datetime = dt
                event.close_at = close_at
                Event.objects.save([event], allowed_types)

            # redirect to this same view resets the form values
            return redirect('em.events.new.month', year=year, month=month)

    view_name = 'em.events.new.month'
    context = {
        'year': year,
        'month': month,
        'form': form,
        'calendar_items': Event.objects.small_calendar_items_for_month(year, month),
        'calendar_options': {
            'buttons': True,
            'next_url': utils.next_url(year, month, view_name),
            'prev_url': utils.prev_url(year, month, view_name),
        }
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def item(request, pk):
    template_name = 'eventManager/events_item.html'
    event = get_object_or_404(Event, pk=pk)

    if request.method == 'DELETE':
        Event.objects.delete([event])
        return htmx_redirect(reverse('em.events.calendar'))

    if request.method == 'POST':

        if request.POST.get('action') == 'set_public':
            Event.objects.set_public([event])
        elif request.POST.get('action') == 'set_private':
            Event.objects.set_private([event])

        elif request.POST.get('action') == 'set_active':
            Event.objects.set_active([event])
        elif request.POST.get('action') == 'set_inactive':
            Event.objects.set_inactive([event])

        return htmx_refresh()

    context = {
        'event': event,
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def edit(request, pk):
    template_name = 'eventManager/events_edit.html'
    event = get_object_or_404(Event, pk=pk)

    if request.method == 'GET':
        form = EventForm(instance=event)

    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)
        if form.is_valid():
            event = form.save(commit=False)
            Event.objects.save([event])
            form.save_m2m()  # saves many-to-many relation after event saves
            url = reverse('em.events.item', args=[event.pk])
            return redirect(url)

    context = {
        'form': form,
        'event': event,
    }
    return render(request, template_name, context)
