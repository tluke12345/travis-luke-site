from django.contrib import messages as notify
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import reverse

from apps.eventManager.decorators import is_manager
from apps.core.http import htmx_redirect
from apps.core.utils import paginate
from apps.eventManager.filters import MessageFilter

from apps.message.models import Message


@user_passes_test(is_manager)
def index(request):
    template_name = 'eventManager/messages_index.html'

    query = request.GET.copy()
    page = 1
    if 'page' in query:
        page = int(query.pop('page')[0])

    messages = Message.objects.all()
    f = MessageFilter(query, messages)

    context = {
        'message_list': paginate(f.qs, page),
        'filter': f,
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def item(request, pk):
    template_name = 'eventManager/messages_item.html'
    message = get_object_or_404(Message, pk=pk)
    referer = reverse('em.messages.index')

    if request.method == "DELETE":
        Message.objects.delete(message)
        notify.error(request, 'Deleted message')
        return htmx_redirect(referer)

    if request.method == 'POST':
        if request.POST.get('action') == 'ignore':
            Message.objects.ignore_sender(message)
            notify.error(request, 'Ignored message')
            return htmx_redirect(referer)

    context = {
        'message': message,
    }
    return render(request, template_name, context)
