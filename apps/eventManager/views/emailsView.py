from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.shortcuts import reverse

from apps.eventManager.filters import EmailReceiptFilter
from apps.core.utils import paginate

from apps.email.decorators import is_email_user
from apps.email.utils import render_template
from apps.event.management.commands import send_reminders

from apps.email.models import EmailReceipt
from apps.event.models import Reservation
from apps.event.models import Event
from apps.message.models import Message


@user_passes_test(is_email_user)
def preview_html(request, name):
    template_name = f'email/{name}.html'

    context = {
        'reservation': Reservation.objects.example(),
        'old_event': Event.objects.example(),
        'message': Message.objects.example(),
        'summary': send_reminders.example_results(),
    }
    return render(request, template_name, context)


@user_passes_test(is_email_user)
def preview(request):
    template_name = 'eventManager/emails_preview.html'

    # (<label name>, <template name (without '.html')>)
    html_emails = [
        ('email.reservation.confirmation', 'email_reservation_confirmation'),
        ('email.reservation.reminder', 'email_reservation_reminder'),
        ('em.reminder.summary', 'em_email_reminder_summary'),
        ('em.reservation.received', 'em_email_reservation_received'),
        ('em.reservation.moved', 'em_email_reservation_moved'),
        ('em.reservation.canceled', 'em_email_reservation_canceled'),
        ('em.message', 'em_email_message_received'),
    ]

    # (<label name>, <template name (WITH '.txt')>)
    text_emails = [
        ('email.reservation.confirmation', 'email_reservation_confirmation.txt'),
        ('email.reservation.reminder', 'email_reservation_reminder.txt'),
    ]

    # Render text emails
    context = {
        'reservation': Reservation.objects.example(),
    }
    text_emails = [
        (name, render_template(f'email/{template}', context)) for
        name, template in
        text_emails
    ]

    context = {
        'html_emails': html_emails,
        'text_emails': text_emails,
    }
    return render(request, template_name, context)


@user_passes_test(is_email_user)
def index(request):
    template_name = 'eventManager/emails_index.html'
    query = request.GET.copy()
    page = 1
    if 'page' in query:
        page = int(query.pop('page')[0])

    receipts = EmailReceipt.objects.all()
    f = EmailReceiptFilter(query, receipts)

    context = {
        'email_receipts': paginate(f.qs, page),
        'filter': f,
    }
    return render(request, template_name, context)


@user_passes_test(is_email_user)
def item(request, pk):
    template_name = 'eventManager/emails_item.html'
    receipt = get_object_or_404(EmailReceipt, pk=pk)
    referer = request.GET.get('referer', reverse('em.emails.index'))

    if request.method == 'DELETE':
        receipt.delete()

        response = HttpResponse()
        response['HX-Redirect'] = referer
        return response

    context = {
        'email_receipt': receipt,
    }
    return render(request, template_name, context)
