from django.contrib.auth.decorators import user_passes_test
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import render

from apps.event.models import Location

from apps.eventManager.decorators import is_manager
from apps.core.utils import paginate
from apps.eventManager.forms.locationForm import LocationForm


@user_passes_test(is_manager)
def new(request):
    template_name = 'eventManager/includes/locations_new.html'
    if request.method == "GET":
        form = LocationForm()

    if request.method == "POST":
        form = LocationForm(request.POST)
        if form.is_valid():
            location = form.save()
            template_name = 'eventManager/includes/locations_row.html'
            return render(request, template_name, {'location': location})

    context = {
        'form': form,
    }
    return render(request, template_name, context)


@user_passes_test(is_manager)
def item(request, pk):
    location = get_object_or_404(Location, pk=pk)

    if request.method == "DELETE":
        location.delete()

    return HttpResponse("")


@user_passes_test(is_manager)
def index(request):
    """ locations/ """
    template_name = 'eventManager/locations_index.html'
    locations = Location.objects.all()
    context = {
        'locations': paginate(locations, request.GET.get('page')),
    }

    if request.method == "GET":
        page = request.GET.get('page')
        if page:
            template_name = 'eventManager/includes/locations_page.html'

    return render(request, template_name, context)


@user_passes_test(is_manager)
def search(request):
    template_name = 'eventManager/includes/locations_page.html'
    locations = Location.objects.search(request.POST.get('search'))
    context = {
        'locations': paginate(locations, request.GET.get('page')),
    }
    return render(request, template_name, context)
