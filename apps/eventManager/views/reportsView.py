from apps.calendar import models
import datetime
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render

from apps.eventManager.forms.reportForm import ReportForm
from apps.eventManager.exporters.event_csv import event_csv_response
from apps.eventManager.exporters.income_csv import income_csv_response

from apps.eventManager.decorators import is_manager


@user_passes_test(is_manager)
def index(request):
    template_name = 'eventManager/reports_index.html'
    form = ReportForm()

    if request.POST.get('action') == 'generate_csv':
        year = request.POST['year']
        month = request.POST['month']

        if request.POST['report_type'] == 'events':
            return event_csv_response(year, month)
        if request.POST['report_type'] == 'income':
            return income_csv_response(year, month)

    context = {
        'form': form,
    }
    return render(request, template_name, context)
