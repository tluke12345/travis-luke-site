import csv
from django.http import HttpResponse
from django.utils import timezone
from apps.event.models import Event


def event_csv_response(year, month):
    response = HttpResponse(content_type='text/csv')
    
    filename = f'events_{year}-{month}.csv'
    response['Content-Disposition'] = f'attachment; filename="{filename}"'

    writer = csv.writer(response)
    writer.writerow([
        'Date',
        'Time',
        'Was Held',
        'Type',
        '# Customers',
        'Income',
        'Hours'
    ])

    events = Event.objects.for_month(year, month)
    for event in events:
        dt = timezone.localtime(event.datetime)

        customer_count = event.active_reservations.count()
        price = event.price
        income = 0
        if customer_count > 0 and price:
            income = customer_count * event.price

        was_held = False
        if event.is_active and event.is_past and customer_count > 0:
            was_held = True

        hours = 0
        if was_held:
            hours = 1

        writer.writerow([
            f'{dt: %Y/%m/%d}',
            f'{dt: %H:%M}',
            str(was_held),
            event.current_event_type_name(),
            str(customer_count),
            str(income),
            str(hours),
        ])

    return response
