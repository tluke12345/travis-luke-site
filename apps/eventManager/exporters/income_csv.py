import csv
from django.http import HttpResponse
from django.utils import timezone
from apps.event.models import Event


def income_csv_response(year, month):
    response = HttpResponse(content_type='text/csv')

    filename = f'income_{year}-{month}.csv'
    response['Content-Disposition'] = f'attachment; filename="{filename}"'

    writer = csv.writer(response)
    writer.writerow([
        'Date',
        'Time',
        'Customer',
        'Amount',
        'Method'
    ])

    events = Event.objects.for_month(year, month)
    for event in events:
        dt = timezone.localtime(event.datetime)

        reservations = event.active_reservations
        for reservation in reservations:

            writer.writerow([
                f'{dt: %Y/%m/%d}',
                f'{dt: %H:%M}',
                reservation.name,
                event.price,
                'Line'
            ])

    return response
