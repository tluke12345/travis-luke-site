import datetime
from django.utils import timezone


def time_from_string(time):
    if type(time) == type(datetime.time):
        return time

    time = time.split(':')
    hour = int(time[0])
    minute = 0
    second = 0
    if len(time) > 1:
        minute = int(time[1])
    if len(time) > 2:
        second = int(time[2])
    return datetime.time(hour, minute, second, 0)


def calc_datetimes(year, month, days, times):
    cleaned = []
    for day in days:
        for time in times:

            # wrap in try if any problem
            time = time_from_string(time)
            dt = datetime.datetime(
                year,
                month,
                int(day),
                time.hour,
                time.minute,
                time.second,
                0,
            )

            cleaned.append(timezone.make_aware(dt))
    return cleaned


def calc_close_datetime(start, day_delta, time):
    delta = datetime.timedelta(days=day_delta)
    return (start + delta).replace(hour=time.hour, minute=time.minute)


def customer_dictionaries(reservations):
    """
    Return customer dictionaries for given reservations
    """
    by_email = {}
    for r in reservations:
        customer = by_email.get(r.email, {})
        customer.setdefault('email', r.email)
        customer.setdefault('names', set()).add(r.name)
        customer.setdefault('reservations', []).append(r)
        customer.setdefault('id', r.pk)
        by_email[r.email] = customer
    return list(by_email.values())
