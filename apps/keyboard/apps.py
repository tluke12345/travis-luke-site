from django.apps import AppConfig


class KeyboardConfig(AppConfig):
    name = 'apps.keyboard'
