// MASK KEYS
const META = "meta";
const ALT = "alt";
const CTRL = "ctrl";
const SHIFT = "shift";

const CONTROLS = [
  { id: "control_shift", hint: "s", mask: SHIFT },
  { id: "control_alt", hint: "a", mask: ALT },
  { id: "control_ctrl", hint: "c", mask: CTRL },
  { id: "control_meta", hint: "m", mask: META },
];

// DWM
const BINDINGS = [
  /* -------------------------------------------------------------------------*/
  // DWM -APPS
  /* ------------------------------------------------------------------------ */
  {
    name: "terminal",
    key: "enter",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "firefox",
    key: "u",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "dmenu",
    key: "p",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "code",
    key: "i",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "dmenu->notes",
    key: "o",
    scope: "dwm",
    masks: [META],
  },
  /* --------------------------*/
  // DWM -> dwm Functions
  {
    name: "Toggle Statusbar",
    key: "b",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "Scratchpad Terminal",
    key: "`",
    scope: "dwm",
    masks: [META],
  },

  /* --------------------------*/
  // DWM -> Management
  {
    name: "Quit Program",
    key: "q",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "Shutdown Menu",
    key: "q",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "Reload DWM",
    key: "q",
    scope: "dwm",
    masks: [META, SHIFT, CTRL],
  },
  /* --------------------------*/
  // DWM - MOVEMENT
  {
    name: "Next window",
    key: "j",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "Prev window",
    key: "k",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "Move Window +1",
    key: "j",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "Move Window -1",
    key: "k",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "tag 1",
    key: "1",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "tag 2",
    key: "2",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "tag 3",
    key: "3",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "tag 4",
    key: "4",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "tag 5",
    key: "5",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "tag 6",
    key: "6",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "tag 7",
    key: "7",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "tag 8",
    key: "8",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "tag 9",
    key: "9",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "move -> tag 1",
    key: "1",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> tag 2",
    key: "2",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> tag 3",
    key: "3",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> tag 4",
    key: "4",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> tag 5",
    key: "5",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> tag 6",
    key: "6",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> tag 7",
    key: "7",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> tag 8",
    key: "8",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> tag 9",
    key: "9",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  /* --------------------------*/
  // DWM - MONITORS
  {
    name: "next Monitor",
    key: ",",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "prev Monitor",
    key: ".",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "move -> next Monitor",
    key: ",",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "move -> prev Monitor",
    key: ".",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  /* --------------------------*/
  // DWM - LAYOUT
  {
    name: "layout 0 -> Tile",
    key: "n",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "layout 1 -> full screen",
    key: "m",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "toggle floating",
    key: "f",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "Increase Primary window width",
    key: "l",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "Decrease Primary window width",
    key: "h",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "Master column +1",
    key: "h",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  {
    name: "Master column -1",
    key: "l",
    scope: "dwm",
    masks: [META, SHIFT],
  },
  /* --------------------------*/
  // DWM - MOUSE
  {
    name: "Move Floating Window",
    key: "mouse1",
    scope: "dwm",
    masks: [META],
  },
  {
    name: "resize Floating Window",
    key: "mouse1",
    scope: "dwm",
    masks: [META, SHIFT],
  },

  /* -------------------------------------------------------------------------*/
  // FIREFOX
  /* -------------------------------------------------------------------------*/
  {
    name: "focus url bar",
    key: "l",
    scope: "firefox",
    masks: [CTRL],
  },
  {
    name: "back",
    key: "left",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "forward",
    key: "right",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "bookmarks sidebar",
    key: "b",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "history sidebar",
    key: "s",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "lastpass",
    key: "w",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "new tab",
    key: "t",
    scope: "firefox",
    masks: [CTRL],
  },
  {
    name: "close tab",
    key: "w",
    scope: "firefox",
    masks: [CTRL],
  },
  {
    name: "tab chooser",
    key: "tab",
    scope: "firefox",
    masks: [CTRL],
  },
  {
    name: "1st tab",
    key: "1",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "2nd tab",
    key: "2",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "3rd tab",
    key: "3",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "4th tab",
    key: "4",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "5th tab",
    key: "5",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "6th tab",
    key: "6",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "7th tab",
    key: "7",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "8th tab",
    key: "8",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "9th tab",
    key: "9",
    scope: "firefox",
    masks: [ALT],
  },
  {
    name: "Screenshot Tool",
    key: "s",
    scope: "firefox",
    masks: [CTRL, SHIFT],
  },

  /* -------------------------------------------------------------------------*/
  // VSCODE
  /* -------------------------------------------------------------------------*/
  {
    name: "Goto Symbol (Navigate by Outline)",
    key: ".",
    scope: "code",
    masks: [CTRL, SHIFT],
  },
  {
    name: "command prompt -> search files",
    key: "f1",
    scope: "code",
  },
  {
    name: "command prompt -> settings",
    key: "f1",
    keys_after: [">"],
    scope: "code",
  },
  {
    name: "command prompt -> search file symbols",
    key: "f1",
    keys_after: ["@"],
    scope: "code",
  },
  {
    name: "command prompt -> Search project symbols",
    key: "f1",
    keys_after: ["#"],
    scope: "code",
  },
  {
    name: "Rename Variable (in scope)",
    key: "f2",
    scope: "code",
  },
  {
    name: "Find in All Files",
    key: "f",
    scope: "code",
    masks: [CTRL, SHIFT],
  },
  {
    name: "toggle terminal",
    key: "`",
    scope: "code",
    masks: [CTRL],
  },
  {
    name: "focus sidebar",
    key: "1",
    scope: "code",
    masks: [CTRL],
  },
  {
    name: "focus left editor group",
    key: "2",
    scope: "code",
    masks: [CTRL],
  },
  {
    name: "focus right editor group",
    key: "3",
    scope: "code",
    masks: [CTRL],
  },
  {
    name: "move -> right editor group",
    key: "right",
    scope: "code:editor",
    masks: [CTRL],
  },
  {
    name: "move -> left editor group",
    key: "left",
    scope: "code:editor",
    masks: [CTRL],
  },
  {
    name: "next editor group tab",
    key: "l",
    scope: "code:editor",
    masks: [CTRL],
  },
  {
    name: "prev editor group tab",
    key: "h",
    scope: "code:editor",
    masks: [CTRL],
  },
  {
    name: "move editor tab -> right",
    key: "l",
    scope: "code:editor",
    masks: [CTRL, SHIFT],
  },
  {
    name: "move editor tab -> left",
    key: "h",
    scope: "code:editor",
    masks: [CTRL, SHIFT],
  },
  {
    name: "reset editor font zoom",
    key: "0",
    scope: "code",
    masks: [CTRL],
  },
  {
    name: "editor font zoom OUT",
    key: "-",
    scope: "code",
    masks: [CTRL],
  },
  {
    name: "editor font zoom IN",
    key: "=",
    scope: "code:editor",
    masks: [CTRL],
  },
  {
    name: "reset UI zoom",
    key: "0",
    scope: "code",
    masks: [CTRL, SHIFT],
  },
  {
    name: "UI zoom OUT",
    key: "-",
    scope: "code",
    masks: [CTRL, SHIFT],
  },
  {
    name: "UI zoom IN",
    key: "=",
    scope: "code",
    masks: [CTRL, SHIFT],
  },
  {
    name: "close editor window",
    key: "w",
    scope: "code:editor",
    masks: [ALT],
  },
  {
    name: "close all editor windows",
    key: "w",
    scope: "code:editor",
    masks: [ALT, SHIFT],
  },
  {
    name: "toggle sidebar visibility",
    key: "b",
    scope: "code",
    masks: [CTRL, SHIFT],
  },
  {
    name: "focus next(down) sidebar tab",
    key: "j",
    scope: "code:sidebar",
    masks: [CTRL],
  },
  {
    name: "focus prev(up) sidebar tab",
    key: "k",
    scope: "code:sidebar",
    masks: [CTRL],
  },
  {
    name: "new file",
    key: "n",
    scope: "code:file explorer",
    masks: [CTRL],
  },
  {
    name: "new folder",
    key: "n",
    scope: "code:file explorer",
    masks: [CTRL, SHIFT],
  },
  {
    name: "Find (in file)",
    key: "\\",
    keys_after: ["Ctrl + f"],
    scope: "code:editor",
    masks: [CTRL],
  },
  {
    name: "multi-cursor: select next",
    key: "n",
    scope: "code:editor",
    masks: [CTRL, ALT, SHIFT],
  },
  {
    name: "multi-cursor: select all",
    key: "a",
    scope: "code:editor",
    masks: [CTRL, ALT, SHIFT],
  },
  {
    name: "move current line up one",
    key: "up",
    scope: "code:editor",
    masks: [ALT],
  },
  {
    name: "move current line down one",
    key: "down",
    scope: "code:editor",
    masks: [ALT],
  },

  /* -------------------------------------------------------------------------*/
  // VIM
  /* -------------------------------------------------------------------------*/
  {
    name: "Move cursor -> (u)p -> up half page",
    key: "u",
    scope: "vim",
    masks: [CTRL],
  },
  {
    name: "Move cursor -> (d)own -> down half page",
    key: "d",
    scope: "vim",
    masks: [CTRL],
  },
  {
    name: "Move cursor -> (b)ack -> up 1 page",
    key: "b",
    scope: "vim",
    masks: [CTRL],
  },
  {
    name: "Move cursor -> (f)orward -> down 1 page",
    key: "f",
    scope: "vim",
    masks: [CTRL],
  },
  {
    name: "Move cursor -> (H)igh -> top of screen",
    key: "h",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "Move cursor -> (M)iddle -> middle of screen",
    key: "m",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "Move cursor -> (L)ow -> bottom of screen",
    key: "l",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "Scroll screen -> so cursor is top",
    key: "z",
    keys_after: ["t"],
    scope: "vim",
  },
  {
    name: "Scroll screen -> so cursor is bottom",
    key: "z",
    keys_after: ["b"],
    scope: "vim",
  },
  {
    name: "Scroll screen -> so cursor is middle",
    key: "z",
    keys_after: ["z"],
    scope: "vim",
  },
  {
    name: "move cursor -> line #",
    key: "g",
    keys_before: ["#"],
    scope: "vim",
  },
  {
    name: "move cursor -> document start",
    key: "g",
    keys_after: ["g"],
    scope: "vim",
  },
  {
    name: "move cursor -> document end",
    key: "g",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "next word",
    key: "w",
    scope: "vim",
  },
  {
    name: "end of word",
    key: "e",
    scope: "vim",
  },
  {
    name: "back word",
    key: "b",
    scope: "vim",
  },
  {
    name: "next word (ignore punctuation)",
    key: "w",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "end of word (ignore punctuation)",
    key: "e",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "back word (ignore punctuation)",
    key: "b",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "start of line",
    key: "0",
    scope: "vim",
  },
  {
    name: "end of line",
    key: "4",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "start of line (ignore whitespace)",
    key: "6",
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "cursor history -> back 1 position",
    key: "o",
    scope: "vim",
    masks: [CTRL],
  },
  {
    name: "cursor history -> previous position",
    key: "`",
    keys_after: ["`"],
    scope: "vim",
  },
  {
    name: "copy to system clipboard",
    key: "y",
    keys_before: ['"', "+"],
    scope: "vim",
  },
  {
    name: "paste from system clipboard",
    key: "p",
    keys_before: ['"', "+"],
    scope: "vim",
  },
  {
    name: "mark -> mark current line with label <key>",
    key: "m",
    keys_after: ["<key>"],
    scope: "vim",
  },
  {
    name: "mark -> move cursor to mark labeled <key>",
    key: "'",
    keys_after: ["<key>"],
    scope: "vim",
  },
  {
    name: "indent selection",
    key: ".",
    scope: "vim:visual",
    masks: [SHIFT],
  },
  {
    name: "outdent selection",
    key: ",",
    scope: "vim:visual",
    masks: [SHIFT],
  },
  {
    name: "indent # lines",
    key: ".",
    keys_after: ["#", "enter"],
    scope: "vim",
    masks: [SHIFT],
  },
  {
    name: "outdent # lines",
    key: ",",
    keys_after: ["#", "enter"],
    scope: "vim",
    masks: [SHIFT],
  },
];
