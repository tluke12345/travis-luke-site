// ---------------------------------------------------------------------------
// GLOBAL
// ---------------------------------------------------------------------------
const ID = {
  keyboard: 'keyboard',
  control__scope: 'control__scope',
  control__mask: 'control__mask',
  info: 'info',
}
const CSS = {
  key__base: 'key',
  key__bound: 'key--bound',
  key__inscope: 'key--inscope',
  key__partial: 'key--partial',
  key__match: 'key--match',
  key__selected: 'key--selected',
}

// Legacy support for css class add/remove
// ---------------------------------------------------------------------------
HTMLElement.prototype.addClass = function (className) {
  var arr = this.className.split(' ')
  if (arr.indexOf(className) == -1) {
    this.className += ' ' + className
  }
}
HTMLElement.prototype.removeClass = function (className) {
  var classes = this.className.split(' ')
  var newClassName = ''
  var i
  for (i = 0; i < classes.length; i++) {
    if (classes[i] !== className) {
      newClassName += classes[i] + ' '
    }
  }
  this.className = newClassName
}

// ---------------------------------------------------------------------------
// MAIN CODE
// ---------------------------------------------------------------------------
;(function () {
  let currentMask = []
  let currentScope = null
  let currentKey = null
  let masks = []
  let scopes = []
  let keyboard = []
  let keys = []

  // PARSE - read user defined KEYBOARD and BINDINGS
  // ---------------------------------------------------------------------------
  KEYBOARD.forEach((KEYBOARD_row) => {
    let row = []

    // Calculate key width
    let row_width = 0
    KEYBOARD_row.forEach((key) => (row_width += key.width || 1.0))
    base_width = 1000 / KEYBOARD_WIDTH

    KEYBOARD_row.forEach((key) => {
      if (key.id === undefined && key.key) key.id = 'key_' + key.key
      key.width = Math.round(base_width * (key.width || 1)) / 10
      key.bindings = []
      row.push(key)
      keys.push(key)
    })
    keyboard.push(row)
  })
  BINDINGS.forEach((b) => {
    const binding_masks = b.masks || []
    const binding_scope = b.scope ? b.scope.split(':')[0] : null

    binding_masks.forEach((m) => {
      if (masks.indexOf(m) == -1) masks.push(m)
    })
    if (binding_scope) {
      if (scopes.indexOf(binding_scope) == -1) scopes.push(binding_scope)
    }

    const key = keys.filter((k) => k.key == b.key)[0]
    if (key) key.bindings.push(b)
  })

  // BUILD - code generated HTML
  // ---------------------------------------------------------------------------
  let keyboardHTML = ''
  keyboard.forEach((row) => {
    keyboardHTML += '<div class="row">\n'
    row.forEach((key) => {
      if (!key.key) {
        keyboardHTML += `\n<div style="width: ${key.width}%"></div>`
      } else {
        keyboardHTML += `
          <div class="key__container" style="width: ${key.width}%">
            <div class="${CSS.key__base}" id="${key.id}">${key.key}</div>
          </div>`
      }
    })
    keyboardHTML += '</div>\n'
  })
  document
    .getElementById(ID.keyboard)
    .insertAdjacentHTML('beforeend', keyboardHTML)

  let maskControlsHTML = ''
  masks.forEach((m) => {
    maskControlsHTML += `
      <input class="control__mask_input" type="checkbox" id="control_${m}" />
      <label class="control__mask_label" for="control_${m}">${m}</label>`
  })
  document
    .getElementById(ID.control__mask)
    .insertAdjacentHTML('beforeend', maskControlsHTML)

  let scopeControlHTML = ''
  scopes.forEach((s) => {
    scopeControlHTML += `\n<option value="${s}">${s}</option>`
  })
  document
    .getElementById(ID.control__scope)
    .insertAdjacentHTML('beforeend', scopeControlHTML)

  // EVENTS - keys and controls
  // ---------------------------------------------------------------------------
  keys.forEach((key) => {
    element = document.getElementById(key.id)
    if (element) element.addEventListener('click', handleKeyClick)
  })
  masks.forEach((m) => {
    element = document.getElementById('control_' + m)
    if (element) element.addEventListener('change', handleMaskChange)
  })
  document
    .getElementById(ID.control__scope)
    .addEventListener('change', handleScopeChange)

  // ---------------------------------------------------------------------------
  // HANDLERS
  // ---------------------------------------------------------------------------
  function handleKeyClick(event) {
    const element = event.target
    let selectable = false
    selectable = selectable || element.className.indexOf(CSS.key__match) > 1
    selectable = selectable || element.className.indexOf(CSS.key__partial) > 1
    selectable = selectable || element.className.indexOf(CSS.key__bound) > 1
    selectable = selectable || element.className.indexOf(CSS.key__inscope) > 1
    if (!selectable) return

    // Unselect
    selected = document.getElementsByClassName(CSS.key__selected)[0]
    if (selected) selected.removeClass(CSS.key__selected)
    if (element == selected) {
      currentKey = null
      updateInfo()
    } else {
      // Select
      event.target.addClass(CSS.key__selected)
      key = keys.filter((k) => k.id === event.target.id)[0]
      currentKey = key
      updateInfo()
    }
  }
  function handleMaskChange(event) {
    currentMask = []
    masks.forEach((m) => {
      control = document.getElementById('control_' + m)
      if (control.checked) currentMask.push(m)
    })
    updateUI()
    updateInfo()
  }
  function handleScopeChange(event) {
    currentScope = event.target.value || null
    updateUI()
    updateInfo()
  }

  // ---------------------------------------------------------------------------
  // UTIL
  // ---------------------------------------------------------------------------
  function isMatch(masks) {
    masks = masks || []
    let match = true
    if (masks.length !== currentMask.length) {
      match = false
    }
    currentMask.forEach((str) => {
      if (masks.indexOf(str) === -1) {
        match = false
      }
    })
    return match
  }
  function isPartial(masks) {
    let matched = false
    masks = masks || []
    masks.forEach((str) => {
      if (!matched && currentMask.indexOf(str) !== -1) matched = true
    })
    currentMask.forEach((str) => {
      if (!matched && masks.indexOf(str) !== -1) matched = true
    })
    return matched
  }
  function isScope(scope) {
    if (currentScope == null) return true
    const domain = scope.split(':')[0]
    return domain == currentScope
  }

  // ---------------------------------------------------------------------------
  // UI
  // ---------------------------------------------------------------------------
  function updateUI() {
    keys.forEach((key) => {
      const element = document.getElementById(key.id)
      if (element && key.bindings) {
        const match = key.bindings.filter((b) => {
          return isMatch(b.masks) && isScope(b.scope)
        })
        const partial = key.bindings.filter((b) => {
          return isPartial(b.masks) && isScope(b.scope)
        })
        const inScope = key.bindings.filter((b) => {
          return key.bindings.length > 0 && isScope(b.scope)
        })

        element.className = CSS.key__base
        if (match.length > 0) {
          element.addClass(CSS.key__match)
        } else if (partial.length > 0) {
          element.addClass(CSS.key__partial)
        } else if (inScope.length > 0) {
          element.addClass(CSS.key__inscope)
        } else if (key.bindings.length > 0) {
          element.addClass(CSS.key__bound)
        }

        if (key == currentKey) {
          element.addClass(CSS.key__selected)
        }
      }
    })
  }
  function updateInfo() {
    info_element = document.getElementById(ID.info)
    info_bindings = currentKey && currentKey.bindings ? currentKey.bindings : []
    info_length = Math.max(4, info_bindings.length)

    innerHTML = ''
    for (let idx = 0; idx < info_length; idx++) {
      if (idx < info_bindings.length) {
        b = info_bindings[idx]

        let match_css_class = ''
        if (isMatch(b.masks) && isScope(b.scope)) {
          match_css_class = ' command--match'
        } else if (isPartial(b.masks) && isScope(b.scope)) {
          match_css_class = ' command--partial'
        }

        if (!isScope(b.scope)) {
          match_css_class += ' command--inactive'
        }

        // Build mask buttons
        let keysHTML = ''
        let bind_keys = []
        if (b.masks) {
          bind_keys.push.apply(bind_keys, b.masks)
        }
        if (b.keys_before) {
          bind_keys.push.apply(bind_keys, b.keys_before)
        }
        if (b.key) {
          bind_keys.push(b.key)
        }
        if (b.keys_after) {
          bind_keys.push.apply(bind_keys, b.keys_after)
        }
        bind_keys.forEach((str) => {
          keysHTML += `\n<span class="command__key">${str}</span>`
        })

        innerHTML += `
          <div class="command${match_css_class}">
            <div class="command__key_container">${keysHTML}</div>
            <div class="command__name">${b.name} [${b.scope || 'Global'}]</div>
          </div>
        `
      } else {
        innerHTML += '\n<div class="command"></div>'
      }
    }
    info_element.innerHTML = innerHTML
    return
  }

  // ---------------------------------------------------------------------------
  // INIT
  // ---------------------------------------------------------------------------
  updateUI()
  updateInfo()
})()
