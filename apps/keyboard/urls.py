from django.urls import path
from apps.keyboard.views import index

urlpatterns = [

    path(
        '',
        index,
        name="keyboard.index",
    ),
]
