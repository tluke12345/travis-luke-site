# Generated by Django 3.1.6 on 2021-04-27 06:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0004_auto_20210427_1525'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reservation',
            name='contact_preference',
        ),
        migrations.RemoveField(
            model_name='reservation',
            name='location_preference',
        ),
        migrations.RemoveField(
            model_name='reservation',
            name='payment_preference',
        ),
        migrations.RemoveField(
            model_name='reservation',
            name='software_preference',
        ),
    ]
