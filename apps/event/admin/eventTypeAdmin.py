from django.contrib import admin

from apps.event.models import EventType


@admin.register(EventType)
class EventTypeAdmin(admin.ModelAdmin):
    pass
