# from django.db.models import Sum
from django.contrib import admin
from django.utils import timezone

from apps.event.models import Event
from apps.event.models import Reservation
from apps.event.utils import status_for_event


class ReservationInline(admin.TabularInline):
    '''Stacked Inline View for Reservation'''

    model = Reservation
    fields = ['name']
    show_change_link = True
    min_num = 0
    extra = 0


# EVENT ACTIONS ---------------------------------------------------------------
def set_inactive_action(modelAdmin, request, queryset):
    for event in queryset:
        event.is_active = False
        event.save()


def set_active_action(modelAdmin, request, queryset):
    for event in queryset:
        event.is_active = True
        event.save()


def set_not_public_action(modelAdmin, request, queryset):
    for event in queryset:
        event.is_public = False
        event.save()


def set_public_action(modelAdmin, request, queryset):
    for event in queryset:
        event.is_public = True
        event.save()


set_inactive_action.short_description = "Cancel (inactive)"
set_active_action.short_description = "Uncancel (active)"
set_not_public_action.short_description = "Unannounce (not public)"
set_public_action.short_description = "Announce (public)"


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = [
        'date',
        'time',
        'close',
        'event_type',
        'reservation_count',
        'is_public',
        'status_msg',
    ]
    inlines = (
        ReservationInline,
    )
    actions = [
        set_active_action,
        set_inactive_action,
        set_public_action,
        set_not_public_action,
    ]
    date_hierarchy = 'datetime'
    list_filter = ('is_public',)

    def date(self, obj):
        return f'{obj.datetime: %Y/%m/%d}'

    def time(self, obj):
        end = obj.datetime + obj.duration
        return f'{obj.datetime: %H:%M} ~ {end: %H:%M}'

    def close(self, obj):
        now = timezone.now()
        if now > obj.close_at:
            return "Closed"
        else:
            dt = timezone.localtime(obj.close_at)
            remaining = (dt - now).total_seconds() / (60 * 60)
            days = int(remaining // 24)
            hours = int(remaining % 24)
            return f' {days}d {hours}h'

    def reservation_count(self, obj):
        return obj.reservations.count()
    reservation_count.short_description = 'reservations'

    def status_msg(self, obj):
        status = status_for_event(obj)
        return status['msg']
    status_msg.short_description = 'status'
