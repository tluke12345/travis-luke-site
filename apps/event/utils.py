from django.utils import timezone
from django.utils.translation import gettext as _


def status_for_event(event):
    """ 
    dictionary for current event status 

    Format: {
        'state': <'upcoming', 'closed', 'in_progress', 'past'>,
        'attendance': <'empty', 'reserved', 'full'>,
        'is_empty': <True, False>,
        'is_past': <True, False>,
        'is_public': <True, False>,
        'msg': (Localized)<'canceled', pending', 'not held', 'closed', 'in progress', 'finished'>,
        'color': <CSS class name>,
    }
    """
    # (is_active, completion, attendance)
    # (CANCELED, UPCOMING, ANY)             # canceled          red
    # (CANCELED, CLOSED, ANY)               # canceled          red
    # (CANCELED, IN_PROGRESS, ANY)          # canceled          red
    # (CANCELED, PAST, ANY)                 # canceled (hidden) red, ghost

    # (ACTIVE, UPCOMING, RESERVED/FULL)     # confirmed     green
    # (ACTIVE, CLOSED, RESERVED/FULL)       # confirmed     green
    # (ACTIVE, IN_PROGRESS, RESERVED/FULL)  # in_progress   orange
    # (ACTIVE, PAST, RESERVED/FULL)         # finished      orange, ghost

    # (ACTIVE, UPCOMING, EMPTY)             # pending       yellow
    # (ACTIVE, CLOSED, EMPTY)               # not held      red
    # (ACTIVE, IN_PROGRESS, EMPTY)          # not held      red
    # (ACTIVE, PAST, EMPTY)                 # not held      red

    canceled = not event.is_active

    # State -> upcoming / closed / in_progress / past
    now = timezone.now()
    if now < event.close_at:
        state = 'upcoming'
    elif now < event.datetime:
        state = 'closed'
    elif now < event.datetime + event.duration:
        state = 'in_progress'
    else:
        state = 'past'

    # attendance -> no reservation (empty) / reserved / full
    count = event.active_reservations.count()
    if count == 0:
        attendance = 'empty'
    elif count < event.reservation_limit:
        attendance = 'reserved'
    else:
        attendance = 'full'

    is_past = state == 'past'
    is_empty = attendance == 'empty'

    status = {
        'state': state,
        'attendance': attendance,
        'is_empty': is_empty,
        'is_past': is_past,
        'is_public': event.is_public,
    }
    if canceled:
        status['msg'] = _('canceled')
        status['color'] = 'red'

    if not canceled and is_empty:
        if state == 'upcoming':
            status['msg'] = _('pending')
            status['color'] = 'yellow'
        else:
            status['msg'] = _('not held')
            status['color'] = 'red'

    if not canceled and not is_empty:
        if state in ['upcoming', 'closed']:
            status['msg'] = _('confirmed')
            status['color'] = 'green'
        if state == 'in_progress':
            status['msg'] = _('in progress')
            status['color'] = 'orange'
        if state == 'past':
            status['msg'] = _('finished')
            status['color'] = 'orange'

    return status


def reservation_button_message_for_event(event):
    if not event.is_public:
        return _('Private lesson')
    if not event.is_active:
        return _('Lesson canceled')

    now = timezone.now()
    if now > (event.datetime + event.duration):
        return _('Lesson finished')
    if now > event.datetime:
        return _('Lesson in progress')
    if now > event.close_at:
        return _('Reservations are closed')

    if event.active_reservations.count() >= event.reservation_limit:
        return _('Lesson is full')

    return _('Make reservation')
