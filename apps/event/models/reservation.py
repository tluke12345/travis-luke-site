from django.db import models
from django.shortcuts import reverse
from django.utils import timezone
from django.utils.crypto import get_random_string

from .reservation_manager import ReservationManager
from .reservation_manager import PublicReservationManager

from apps.email.models import EmailReceipt


class Reservation(models.Model):

    # meta
    created_at = models.DateTimeField(default=timezone.now)
    token = models.CharField(max_length=256, blank=True)

    # if Truthy, the reservaiton screen will display message to customer
    # manager should set this message if problem with reservation
    problem_message = models.CharField(max_length=256, blank=True)

    # event
    event = models.ForeignKey(
        "Event",
        related_name='reservations',
        on_delete=models.CASCADE
    )
    event_preference = models.ForeignKey(
        "EventType",
        related_name='reservations',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )

    # details
    name = models.CharField(max_length=50, blank=True)
    email = models.EmailField(max_length=254, blank=True)
    wants_reminder = models.BooleanField(default=False)
    note = models.CharField(max_length=512, blank=True)

    # cancel
    canceled_at = models.DateTimeField(null=True, blank=True)
    canceled_msg = models.CharField(max_length=512, blank=True)

    # managers
    objects = ReservationManager()
    public = PublicReservationManager()

    def __str__(self):
        name = self.name or '<no name>'
        email = self.email or 'no email'
        pk = self.pk or 'not commited'
        return f'{name} <{email}> ({pk})'

    def get_absolute_url(self):
        return reverse("em.reservations.item", kwargs={"pk": self.pk})

    def url(self):
        url = reverse('english_reservation', args=[self.pk])
        url += '?token=' + self.token
        return url

    def cancel_url(self):
        url = reverse('english_reservation_cancel', args=[self.pk])
        url += '?token=' + self.token
        return url

    def reschedule_url(self):
        url = reverse('english_reservation_reschedule', args=[self.pk])
        url += '?token=' + self.token
        return url

    def save(self, *args, **kwargs):
        if not self.pk:
            self.token = get_random_string(length=32)
        super(Reservation, self).save(*args, **kwargs)

    @property
    def is_reminded(self):
        return EmailReceipt.objects.reservation_has_receipt_of_type(
            self,
            'reservation.reminder'
        )
