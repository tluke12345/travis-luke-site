from django.db import models

class Expense(models.Model):
    duration = models.DurationField()
    description = models.CharField(max_length=100, blank=True)
    event = models.ForeignKey(
        'Event',
        related_name='hours',
        on_delete=models.CASCADE,
    )