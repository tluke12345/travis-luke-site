from django.db import models
from django.utils.translation import get_language


class OptionMixin(models.Model):
    name_en = models.CharField(max_length=50, blank=True)
    name_ja = models.CharField(max_length=50, blank=True)

    class Meta:
        abstract = True

    def __str__(self):
        if self.name:
            return self.name
        return f'{self.__class__.__name__}:{self.pk or "New"}'

    @property
    def name(self):
        language = get_language()
        if language == 'ja':
            return self.name_ja or self.name_en

        return self.name_en or self.name_ja
