from datetime import datetime
from datetime import timedelta
from django.db import models
from django.utils import timezone

from .event_queryset import EventQuerySet
from apps.event.calendar_items import calendar_item_for_event
from apps.event.calendar_items import manager_calendar_item_for_event
from apps.event.calendar_items import manager_small_calendar_item_for_event


class VisibleEventManager(models.Manager):
    """
    Visible Event Manager

    A 'visible' event is any event that should be shown on the public calendar
    and public is allowed to interact with it
    """

    def get_queryset(self):
        # Visible events are:
        #   - Public events -> visible
        # Exceptions
        #   - Canceled past events -> hidden
        return EventQuerySet(self.model, using=self._db) \
            .public() \
            .exclude_canceled_past() \
            .chronological()

    # --------------------------------------------------------------------------
    # Retrieving events
    # --------------------------------------------------------------------------
    def for_month(self, year, month):
        return self.get_queryset().in_year(year).in_month(month)

    def for_day(self, year, month, day):
        return self.for_month(year, month).on_day(day)

    # --------------------------------------------------------------------------
    # Rescheduling
    # --------------------------------------------------------------------------
    def open_for_reschedule(self):
        events = self.get_queryset().upcoming().active().public()
        result = []
        for event in events:
            # brute-force method but seldom used so probably ok
            if event.allow_public_reservation:
                result.append(event)
        return result

    # --------------------------------------------------------------------------
    # Calendar
    # --------------------------------------------------------------------------
    def next_event_date(self):
        next_event = self.get_queryset().upcoming().first()
        if next_event:
            return next_event.datetime

    def calendar_items_for_month(self, year, month):
        events = self.for_month(year, month)
        return [calendar_item_for_event(e) for e in events]

    def upcoming_events_by_day(self, year, month):
        # we use upcoming_or_today to include todays events event thought they
        # might already be finished
        events = self.for_month(year, month).upcoming_or_today()
        by_day = {}
        for event in events:
            day = timezone.localtime(event.datetime).day
            by_day.setdefault(day, []).append(event)
        return by_day


class EventManager(models.Manager):

    def get_queryset(self):
        return EventQuerySet(self.model, using=self._db) \
            .chronological()

    # --------------------------------------------------------------------------
    # Calendar
    # --------------------------------------------------------------------------
    def calendar_items_for_month(self, year, month):
        events = self.for_month(year, month)
        return [manager_calendar_item_for_event(e) for e in events]

    def small_calendar_items_for_month(self, year, month):
        events = self.for_month(year, month)
        return [manager_small_calendar_item_for_event(e) for e in events]

    # --------------------------------------------------------------------------
    # Retrieving events
    # --------------------------------------------------------------------------
    def example(self):
        return self.get_queryset().last()

    def for_month(self, year, month):
        return self.get_queryset().in_year(year).in_month(month)

    def for_day(self, year, month, day):
        return self.for_month(year, month).on_day(day)

    def for_tomorrow(self):
        dt = timezone.localtime(timezone.now() + timedelta(days=1))
        return self.for_day(dt.year, dt.month, dt.day).active()

    def get_by_pk(self, pk):
        if not pk:
            return None
        event = self.get_queryset().filter(pk=pk).first()
        if not event:
            return None
        return event

    # --------------------------------------------------------------------------
    # Modifying events
    # --------------------------------------------------------------------------
    def save(self, events, allowed_types=None):
        for event in events:
            event.save()
            if allowed_types:
                event.allowed_types.set(allowed_types)
                event.save()

    def delete(self, events):
        for event in events:
            event.delete()

    def set_active(self, events):
        for event in events:
            event.is_active = True
        self.save(events)

    def set_inactive(self, events):
        for event in events:
            event.is_active = False
        self.save(events)

    def set_public(self, events):
        for event in events:
            event.is_public = True
        self.save(events)

    def set_private(self, events):
        for event in events:
            event.is_public = False
        self.save(events)
