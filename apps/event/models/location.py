from django.db import models
from django.db.models import Q

from .optionMixin import OptionMixin

TEST_NAME = 'TEST'


class LocationQuerySet(models.QuerySet):
    pass


class LocationManager(models.Manager):
    def get_queryset(self):
        return LocationQuerySet(self.model, using=self._db)

    def search(self, search_text):
        return self.get_queryset().filter(
            Q(name_en__contains=search_text) |
            Q(name_ja__contains=search_text)
        )

    # --------------------------------------------------------------------------
    # Testing
    # --------------------------------------------------------------------------

    def create_test_items(self, count):
        for i in range(count):
            n = f'{TEST_NAME} ({i})'
            self.get_queryset().create(name_en=n, name_ja=n)
        return count

    def delete_test_items(self):
        locations = self.get_queryset().filter(name_en__startswith=TEST_NAME)
        count = locations.count()
        locations.delete()
        return count


class Location(OptionMixin):

    objects = LocationManager()

    # def get_absolute_url(self):
    #     return reverse("Location_detail", kwargs={"pk": self.pk})
