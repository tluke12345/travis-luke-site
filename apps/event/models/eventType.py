from django.db import models
from .optionMixin import OptionMixin


class EventType(OptionMixin):

    default_price = models.IntegerField(default=2000)
    default_reservation_limit = models.IntegerField(default=1)

    # def get_absolute_url(self):
    #     return reverse("EventType_detail", kwargs={"pk": self.pk})
