import random

from django.apps import apps
from django.db import models
from django.utils import timezone

from .reservation_queryset import ReservationQuerySet


from apps.website.email import send_reservation_confirmation
from apps.eventManager.email import send_reservation_canceled
from apps.eventManager.email import send_reservation_received
from apps.eventManager.email import send_reservation_moved


class ReservationManager(models.Manager):

    def get_queryset(self):
        return ReservationQuerySet(self.model, using=self._db) \
            .chronological()

    def save(self, reservation):
        reservation.save()

    def delete(self, reservation):
        reservation.delete()

    def cancel(self, reservation):
        reservation.canceled_at = timezone.now()
        reservation.canceled_msg = 'Canceled by Manager'
        self.save(reservation)

    def uncancel(self, reservation):
        reservation.canceled_at = None
        self.save(reservation)

    def first_event_preference(self, event):
        reservation = self.get_queryset() \
            .for_event(event) \
            .has_event_preference() \
            .active() \
            .first()
        if reservation:
            return reservation.event_preference

    def active_reservations_for_event(self, event):
        return self.get_queryset() \
            .for_event(event) \
            .active()

    def is_valid_email_for_event(self, event, email):
        # TODO email blacklist check?

        already_reserved = self.get_queryset() \
            .for_event(event) \
            .with_email(email) \
            .count() > 0
        if already_reserved:
            return False

        return True

    def get_by_pk(self, pk):
        if not pk:
            return None

        reservation = self.get_queryset().filter(pk=pk).first()
        return reservation

    def matching_query(self, query):
        if not query:
            return self.get_queryset().all()
        return self.get_queryset().matching_query(query)

    def example(self):
        return self.get_queryset().last()

    def create_test_reservations(self, count):
        Event = apps.get_model('event', 'Event')
        Reservation = apps.get_model('event', 'Reservation')

        emails = [f'test@email{i}.com' for i in range(int(count/3))]
        events = list(Event.objects.all())
        for i in range(count):
            r = Reservation(
                event=random.choice(events),
                name=f'name{i}',
                email=random.choice(emails),
            )
            r.save()
        return count

    def delete_test_reservations(self):
        Reservation = apps.get_model('event', 'Reservation')
        reservations = Reservation.objects.filter(email__startswith="test")
        count = reservations.count()
        reservations.delete()
        return count


class PublicReservationManager(models.Manager):

    def save_new_reservaton(self, reservation):
        reservation.save()
        send_reservation_confirmation(reservation)  # to customer
        send_reservation_received(reservation)      # to admin

    def cancel(self, reservation, msg=None):
        reservation.canceled_at = timezone.now()
        reservation.canceled_msg = msg or ""
        reservation.save()
        send_reservation_canceled(reservation)      # to admin

    def move_to_event(self, reservation, event):
        old_event = reservation.event

        reservation.event.notes += f'MOVED to -> {event}\n    {reservation}\n'
        reservation.event.save()

        event.notes += f'MOVED from <- {reservation.event}\n    {reservation}\n'
        event.save()

        reservation.event = event
        reservation.save()
        send_reservation_moved(reservation, old_event)

    def get_by_pk(self, pk, token):
        if not pk or not token:
            return None

        reservation = self.get_queryset().filter(pk=pk).first()
        if not reservation or reservation.token != token:
            return None
        return reservation
