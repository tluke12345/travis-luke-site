from django.db import models
from django.db.models import Q
from django.utils import timezone


class EventQuerySet(models.QuerySet):
    def chronological(self):
        return self.order_by('datetime')

    def public(self):
        return self.filter(is_public=True)

    def active(self):
        return self.filter(is_active=True)

    def upcoming(self):
        now = timezone.now()
        return self.filter(datetime__gte=now)

    def upcoming_or_today(self):
        today = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
        return self.filter(datetime__gte=today)

    def in_year(self, year):
        return self.filter(datetime__year=year)

    def in_month(self, month):
        return self.filter(datetime__month=month)

    def on_day(self, day):
        return self.filter(datetime__day=day)

    def exclude_canceled_past(self):
        now = timezone.now()
        return self.exclude(
            Q(is_active=False),
            Q(datetime__lt=now),
        )