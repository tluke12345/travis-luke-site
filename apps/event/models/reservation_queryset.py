import datetime
from django.db import models
from django.db.models import Q


class ReservationQuerySet(models.QuerySet):
    def chronological(self):
        return self.order_by('created_at')

    def for_event(self, event):
        return self.filter(event=event)

    def has_event_preference(self):
        return self.filter(event_preference__isnull=False)

    def active(self):
        return self.filter(canceled_at__isnull=True)

    def with_email(self, email):
        return self.filter(email=email)

    def matching_query(self, query):
        return self.filter(
            Q(name__icontains=query) |
            Q(email__icontains=query) |
            Q(note__icontains=query)
        )
