from django.db import models
from django.shortcuts import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from . import Reservation
from apps.event.utils import reservation_button_message_for_event
from apps.event.utils import status_for_event

from .event_manager import EventManager
from .event_manager import VisibleEventManager


class Event(models.Model):

    # -- Meta
    is_active = models.BooleanField(default=True)  # NOT Canceled
    is_public = models.BooleanField(default=True)  # public visible(announced)

    # -- Details
    datetime = models.DateTimeField(db_index=True)
    duration = models.DurationField()
    close_at = models.DateTimeField()

    # -- Limit allowed lesson types for this event
    allowed_types = models.ManyToManyField(
        "EventType",
        related_name='+',
        blank=True
    )

    # -- Options
    event_type = models.ForeignKey(
        "EventType",
        related_name='events',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    location = models.ForeignKey(
        "Location",
        related_name='events',
        on_delete=models.PROTECT,
        null=True,
        blank=True,
    )
    custom_price = models.IntegerField(null=True, blank=True)
    custom_reservation_limit = models.IntegerField(null=True, blank=True)

    # -- Other
    notes = models.TextField(blank=True)

    #  -- MANAGERS
    objects = EventManager()
    visible = VisibleEventManager()

    class Meta:
        ordering = ('-datetime',)

    def __str__(self):
        dt = timezone.localtime(self.datetime)
        time = f'{dt: %Y/%m/%d %H:%M}'
        return f'{self.current_event_type_name()}: {time}'

    def get_absolute_url(self):
        return reverse("em.events.item", kwargs={"pk": self.pk})

    def get_calendar_url(self):
        dt = timezone.localtime(self.datetime)
        url = reverse("em.events.calendar.month", args=[dt.year, dt.month])
        url += f'#day_{dt.day}'
        return url

    # --------------------------------------------------------------------------
    # 'Fat' Model
    # --------------------------------------------------------------------------

    @property
    def price(self):
        if self.custom_price != None:
            return self.custom_price
        elif self.event_type:
            return self.event_type.default_price
        elif self.requested_event_type:
            return self.requested_event_type.default_price

    @property
    def reservation_limit(self):
        if self.custom_reservation_limit:
            return self.custom_reservation_limit
        elif self.event_type:
            return self.event_type.default_reservation_limit
        else:
            return 1

    @property
    def allow_public_reservation(self):
        if not self.is_public:
            return False
        if not self.is_active:
            return False
        if self.active_reservations.count() >= self.reservation_limit:
            return False
        now = timezone.now()
        if now > self.close_at:
            return False
        return True

    @property
    def requested_event_type(self):
        return Reservation.objects.first_event_preference(self)

    @property
    def active_reservations(self):
        return Reservation.objects.active_reservations_for_event(self)

    @property
    def is_past(self):
        return (self.datetime + self.duration) < timezone.now()

    @property
    def reservation_button_message(self):
        return reservation_button_message_for_event(self)

    @property
    def status(self):
        return status_for_event(self)

    def current_event_type_name(self):
        if self.event_type:
            return self.event_type.name
        elif self.requested_event_type:
            return self.requested_event_type.name
        return _('Lesson')
