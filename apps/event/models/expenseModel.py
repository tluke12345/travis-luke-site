from django.db import models

class Expense(models.Model):
    amount = models.PositiveIntegerField(default=0)
    description = models.CharField(max_length=100, blank=True)
    event = models.ForeignKey(
        'Event',
        related_name='expenses',
        on_delete=models.CASCADE,
    )