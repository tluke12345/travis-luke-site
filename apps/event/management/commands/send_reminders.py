import logging
from os import stat

from django.core.management.base import BaseCommand
from django.utils import timezone

from apps.event.models import Reservation
from apps.event.models import Event
from apps.website.email import send_reservation_reminder
from apps.eventManager.email import send_reminders_summary

logger = logging.getLogger('email')


def example_results():
    dt = timezone.now()
    return {
        'date': f'{dt: %Y/%m/%d (%a)}',
        'msg': '2 Events (2 canceled)',
        'total_count': 4,
        'active_count': 2,
        'debug_mode': True,
        'events': [
            {
                'name': 'Lesson: 10:30',
                'link': 'www.travis-luke.com/em/events/20',
                'is_canceled': True,
            },
            {
                'name': 'Lesson: 12:00',
                'link': 'www.travis-luke.com/em/events/21',
                'reservations': [
                    'Ryoko: Sent',
                    'Kishiko: Failed to send',
                    'Toru: Skipped (!want_reminder)',
                ]
            },
            {
                'name': 'Online: 13:00',
                'link': 'www.travis-luke.com/em/events/22',
                'reservations': [
                    'Kumiko: Skipped (!email)',
                    'Sadako: Skipped (already reminded)',
                ]
            },
            {
                'name': 'Lesson: 14:00',
                'link': 'www.travis-luke.com/em/events/23',
                'is_canceled': True,
            },
        ]
    }


class Command(BaseCommand):
    help = 'Send Reminder Emails for the next days events'

    def __init__(self, *args, **kwargs):
        self.date = None
        self.debug_mode = False
        self.msg = ''
        self.events = []
        self.total_count = 0
        self.active_count = 0
        super(Command, self).__init__(*args, **kwargs)

    def add_arguments(self, parser):
        parser.add_argument(
            '--debug_mode',
            action="store_true",
            help="Set mail sending to testing mode"
        )
        parser.add_argument(
            '--summary_email',
            action="store_true",
            help="Send summary email to ADMIN_EMAIL"
        )

    def handle(self, *args, **options):

        self.debug_mode = options.get('debug_mode')
        self._log(
            f'Running "send_reminders" command (debug={self.debug_mode})')

        events = Event.objects.for_tomorrow()

        # Process Each event
        for event in events:

            if not self.date:
                dt = timezone.localtime(event.datetime)
                self.date = f'{dt: %Y/%m/%d (%a)}'

            self.total_count += 1
            if event.active_reservations.count() > 0:
                self.active_count += 1

            self._log(f'Event: {event}')
            self.events.append(self._handle_event(event))

        # Overview Message
        self.msg = self._overview_message(self.total_count, self.active_count)
        self._log(self.msg)

        # Summary Email
        if options.get('summary_email'):
            success = send_reminders_summary(self, test_mode=self.debug_mode)
            self._log(f'Summary Email sent to admin (success={success})')

    def _log(self, msg):
        logger.info(msg)
        print(msg)

    def _overview_message(self, total_count, active_count):
        if total_count == 0:
            return 'No events'
        if active_count == 0:
            return f'All ({total_count}) events canceled'
        canceled_count = total_count - active_count
        return f'{active_count} event(s) ({canceled_count} canceled)'

    def _handle_event(self, event):
        dt = timezone.localtime(event.datetime)
        result = {
            'name': f'{event.current_event_type_name()}: {dt: %H:%M}',
            'link': event.get_absolute_url(),
        }

        # Get active reservations
        reservations = Reservation.objects.active_reservations_for_event(event)
        if reservations.count() == 0:
            self._log('    No active reservations')
            result['is_canceled'] = True
            return result

        # send reminder to each reservation
        result['reservations'] = []
        for reservation in reservations:
            msg = self._send_to_reservation(reservation)
            result['reservations'].append(msg)
            self._log(f'    {msg}')

        return result

    def _send_to_reservation(self, reservation):

        if reservation.canceled_at != None:
            return f'{reservation.name}: Canceled'

        if not reservation.email:
            return f'{reservation.name}: Skipped (!email)'

        if not reservation.wants_reminder:
            return f'{reservation.name}: Skipped (!wants_reminder)'

        if reservation.is_reminded:
            return f'{reservation.name}: Skipped (already reminded)'

        # ATTEMPT TO SEND
        success = send_reservation_reminder(
            reservation,
            test_mode=self.debug_mode
        )
        if success:
            return f'{reservation.name}: Sent'
        else:
            return f'{reservation.name}: Failed to send'
