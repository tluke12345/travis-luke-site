from django.utils import timezone
from django.utils.translation import gettext as _

from .utils import status_for_event


def calendar_item_for_event(event):
    dt = timezone.localtime(event.datetime)
    time = f'{dt: %H:%M}'
    color = status_for_event(event)["color"]
    css = ['calendar__event--big']
    if event.is_past:
        style = ''
        css.append('calendar__event--faded')
    else:
        style = f'background: var(--event-color-{color});'
    return {
        'day': dt.day,
        'style': style,
        'class': ' '.join(css),
        'content': time,
        'verbose_content': f'{event.current_event_type_name()}\n{time}',
        'id': event.pk,
    }


def manager_calendar_item_for_event(event):
    dt = timezone.localtime(event.datetime)
    time = f'{dt: %H:%M}'
    count = event.active_reservations.count()
    total = event.reservations.count()
    count_str = str(count)
    if total != count:
        count_str += f' ({total})'
    color = status_for_event(event)["color"]
    style = []
    css = []
    if not event.is_public:
        css.append('calendar__event--faded')
    if event.is_past:
        style.append('border: none;')
        style.append(f'border-left: 5px solid var(--event-color-{color});')
    else:
        style.append(f'background: var(--event-color-{color});')
    return {
        'day': dt.day,
        'class': ' '.join(css),
        'style': ' '.join(style),
        'content': f'{event.current_event_type_name()}\n{time}\n{count_str}',
        'id': event.pk,
        'button': True,
    }


def manager_small_calendar_item_for_event(event):
    dt = timezone.localtime(event.datetime)
    color = status_for_event(event)["color"]
    style = []
    return {
        'day': dt.day,
        'style': f'background: var(--event-color-{color});',
        'content': f'{dt: %H:%M}',
        'id': event.pk,
        'button': True,
    }
