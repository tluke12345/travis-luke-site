from django import template
from django.utils import timezone

register = template.Library()

MINUTE = 60
HOUR = 3600
DAY = 86400
WEEK = 604800


@register.filter
def event_time_remaining_human(event):
    now = timezone.now()
    start = event.datetime

    # Event finished
    if start < now:
        return ''

    delta = (start - now).total_seconds()
    if delta > WEEK:
        weeks = delta / WEEK
        return f'{round(weeks, 1)} Weeks'

    if delta > DAY:
        days = delta / DAY
        return f'{round(days, 1)} Days'

    if delta > HOUR:
        hours = delta / HOUR
        return f'{round(hours, 1)} Hours'

    minutes = delta / MINUTE
    return f'{round(minutes, 1)} Minutes'
