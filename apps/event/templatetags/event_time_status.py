from django import template
from django.utils import timezone
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def event_time_status(event):
    now = timezone.now()
    start = event.datetime
    end = start + event.duration

    result = ''

    # Upcoming
    if now < start:
        result = '<span class="status status--green">upcoming</span>'

    # In Progress
    elif start < now and now < end:
        result = '<span class="status status--orange">in progress</span>'

    # Finished
    else:
        result = '<span class="status status--gray">finished</span>'

    return mark_safe(result)
