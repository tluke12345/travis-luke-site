from django import template
from django.utils import timezone

register = template.Library()


@register.filter
def event_time_range(event):
    start = timezone.localtime(event.datetime)
    end = start + event.duration
    return f'{start: %H:%M} - {end: %H:%M}'
