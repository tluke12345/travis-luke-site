from django import template
from django.utils import timezone
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def event_close_time_status(event):
    now = timezone.now()
    close = event.close_at

    result = ''

    # Not closed
    if now < close:
        result = '<span class="status status--green">Open</span>'

    # Closed
    else:
        result = '<span class="status status--red">Closed</span>'

    return mark_safe(result)
