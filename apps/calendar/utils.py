from django.utils import timezone
from django.utils.translation import gettext as _
from django.shortcuts import reverse


# ------------------------------------------------------------------------------
# UTILS
# ------------------------------------------------------------------------------
def next_month(year, month):
    if month >= 12:
        return (year + 1, 1)
    else:
        return (year, month + 1)


def prev_month(year, month):
    if month <= 1:
        return (year - 1, 12)
    else:
        return (year, month - 1)


def is_valid_date(year, month):
    if year not in range(2000, 2100):
        return False
    if month not in range(1, 13):
        return False
    return True


def next_url(year, month, view_name):
    return reverse(view_name, args=next_month(year, month))


def prev_url(year, month, view_name):
    return reverse(view_name, args=prev_month(year, month))


# ------------------------------------------------------------------------------
# CLAMPED - Only last month, this month, and next month allowed
# ------------------------------------------------------------------------------
def clamped_is_valid_date(year, month):
    today = timezone.localtime(timezone.now())
    current = (today.year, today.month)

    allowed_list = [current, next_month(*current), prev_month(*current)]
    if (year, month) not in allowed_list:
        return False

    return is_valid_date(year, month)


def clamped_next_url(year, month, view_name):
    today = timezone.localtime(timezone.now())
    request_month = (year, month)
    current_month = (today.year, today.month)

    if request_month in [current_month, prev_month(*current_month)]:
        return next_url(year, month, view_name)


def clamped_prev_url(year, month, view_name):
    today = timezone.localtime(timezone.now())
    request_month = (year, month)
    current_month = (today.year, today.month)

    if request_month in [current_month, next_month(*current_month)]:
        return prev_url(year, month, view_name)
