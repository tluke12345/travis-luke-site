from django.db import models
from django.db.models.functions import ExtractDay


class NamedDayQuerySet(models.QuerySet):

    def in_year(self, year):
        return self.filter(date__year=year)

    def in_month(self, month):
        return self.filter(date__month=month)

    def on_day(self, day):
        return self.filter(date__day=day)
    
    def annotate_day(self):
        return self.annotate(day=ExtractDay('date'))
