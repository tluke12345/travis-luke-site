from django.db import models

from .namedDay_queryset import NamedDayQuerySet


class NamedDayManager(models.Manager):

    def get_queryset(self):
        return NamedDayQuerySet(self.model, using=self._db)

    def for_month_by_day(self, year, month):
        days = self.get_queryset() \
            .in_year(year) \
            .in_month(month) \
            .annotate_day()
        by_day = {}
        for named_day in days:
            by_day.setdefault(named_day.day, []).append(named_day)
        return by_day

    def save(self, namedDay):
        namedDay.save()
