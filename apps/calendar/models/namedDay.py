from django.db import models
from django.utils.translation import get_language

from .namedDay_manager import NamedDayManager


class NamedDay(models.Model):

    date = models.DateField()
    name_en = models.CharField(max_length=50, blank=True)
    name_ja = models.CharField(max_length=50, blank=True)
    disable_calendar = models.BooleanField(default=True)

    objects = NamedDayManager()

    class Meta:
        ordering = ('-date',)

    def __str__(self):
        return f'{self.date: %Y/%m/%d} - {self.name or "-no-name-"}'

    @property
    def name(self):
        language = get_language()
        if language == 'ja':
            return self.name_ja or self.name_en

        return self.name_en or self.name_ja
