from django.contrib import admin

from apps.calendar.models import NamedDay


@admin.register(NamedDay)
class NamedDayAdmin(admin.ModelAdmin):
    list_display = [
        'date',
        'name',
        'disable_calendar'
    ]
    date_hierarchy = 'date'
    list_filter = ('disable_calendar',)
