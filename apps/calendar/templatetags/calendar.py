import calendar as cal
import datetime
from django import template
from django.utils import timezone
from django.utils.translation import gettext as _
from django.utils.translation import get_language

from apps.calendar.models import NamedDay

register = template.Library()

# ------------------------------------------------------------------------------
# Template Tags
# ------------------------------------------------------------------------------


@register.inclusion_tag('calendar/templatetags/calendar.html')
def calendar(year, month, day=None, events=[], options=[]):
    """ Month Calendar """
    return _calendar_context(year, month, day, events, options)


@register.inclusion_tag('calendar/templatetags/calendar.html')
def calendar_dark(year, month, day=None, events=[], options=[]):
    """ Month Calendar (dark mode) """
    options['css'] = 'calendar--dark'
    return _calendar_context(year, month, day, events, options)


# ------------------------------------------------------------------------------
# Internal
# ------------------------------------------------------------------------------
def _days_of_week():
    language = get_language()
    if language == 'ja':
        return list('日月火水木金土')
    return ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']


def _create_days(year, month):
    """ generates array of 'day' dictionaries for given year and month """
    days = []
    namedDays_by_day = NamedDay.objects.for_month_by_day(year, month)
    today = datetime.date.today()
    cal.setfirstweekday(cal.SUNDAY)
    for week in cal.monthcalendar(year, month):
        for _, current_day in enumerate(week):

            day = {
                'events': [],
                'day': current_day
            }

            if current_day != 0:
                date = datetime.date(year, month, current_day)
            else:
                date = None

            day['names'] = []
            namedDays = namedDays_by_day.get(current_day, [])
            for namedDay in namedDays:
                day['names'].append(namedDay.name)
                if namedDay.disable_calendar:
                    day['disabled'] = True

            if not date:
                day['stage'] = 'inactive'
            elif date and date == today:
                day['stage'] = 'today'
            elif date and date < today:
                day['stage'] = 'past'
            else:
                day['stage'] = 'future'

            days.append(day)
    return days


def _calendar_context(year, month, day, events, options):
    """ assemble the context used to render the calendar template """

    # Automatically create title / subtitle using year and month
    title = options.get('title', None)
    subtitle = options.get('subtitle', None)
    if not title or not subtitle:
        if get_language() == 'ja':
            date_str = f'{year}年 {month}月'
        else:
            date_str = f'{list(cal.month_abbr)[month]} {year}'

        if not title:
            title = date_str
        elif not subtitle:
            subtitle = date_str

    # create days and add events
    days = _create_days(year, month)

    # Add events
    for event in events:
        day = next((day for day in days if day['day'] == event['day']), None)
        if day:
            # format content -> array, one item for each line in content
            content = event.get('content', 'Event')
            verbose = event.get('verbose_content', content)
            event['content'] = content.split('\n')
            event['verbose_content'] = verbose.split('\n')
            day['events'].append(event)

    # Add Buttons to each day
    for day in days:
        if day['day'] == 0:
            continue
        if not options.get('buttons'):
            continue

        ignores = options.get('buttons_ignore', [])
        if day['stage'] in ignores:
            continue
        if len(day['events']) == 0 and 'empty' in ignores:
            continue

        day['button'] = True

    return {
        'title': title,
        'subtitle': subtitle,
        'days': days,
        'weekdays': _days_of_week,
        'next_url': options.get('next_url', None),
        'prev_url': options.get('prev_url', None),
        'css': options.get('css', None),
    }
