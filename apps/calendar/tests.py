from django.test import TestCase
from django.utils import timezone

from apps.calendar import utils


class CalendarUtilsTest(TestCase):
    def setUp(self):
        today = timezone.localtime(timezone.now())
        self.current = (today.year, today.month)

    def test_next_month(self):
        self.assertEqual(utils.next_month(2000, 1), (2000, 2))
        self.assertEqual(utils.next_month(2000, 11), (2000, 12))
        self.assertEqual(utils.next_month(2000, 12), (2001, 1))

    def test_prev_month(self):
        self.assertEqual(utils.prev_month(2000, 1), (1999, 12))
        self.assertEqual(utils.prev_month(2000, 2), (2000, 1))
        self.assertEqual(utils.prev_month(2000, 12), (2000, 11))

    def test_is_valid_date(self):
        self.assertEqual(utils.is_valid_date(1999, 1), False)
        self.assertEqual(utils.is_valid_date(2000, 1), True)
        self.assertEqual(utils.is_valid_date(2099, 1), True)
        self.assertEqual(utils.is_valid_date(2100, 1), False)

        self.assertEqual(utils.is_valid_date(2000, -1), False)
        self.assertEqual(utils.is_valid_date(2000, 0), False)
        self.assertEqual(utils.is_valid_date(2000, 1), True)
        self.assertEqual(utils.is_valid_date(2000, 12), True)
        self.assertEqual(utils.is_valid_date(2000, 13), False)

    def test_clamped_is_valid_date(self):
        next_month = utils.next_month(*self.current)
        next_next_month = utils.next_month(*next_month)
        prev_month = utils.prev_month(*self.current)
        prev_prev_month = utils.prev_month(*prev_month)

        self.assertEqual(utils.clamped_is_valid_date(*next_month), True)
        self.assertEqual(utils.clamped_is_valid_date(*next_next_month), False)
        self.assertEqual(utils.clamped_is_valid_date(*prev_month), True)
        self.assertEqual(utils.clamped_is_valid_date(*prev_prev_month), False)
