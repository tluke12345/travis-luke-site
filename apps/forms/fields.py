import datetime
from django.forms.fields import DurationField
from django.forms.fields import IntegerField
from django.forms.widgets import NumberInput
from django.utils.regex_helper import _lazy_re_compile


class MinutesDurationField(DurationField):
    """ Minutes Duration Field
    Duration (datetime.timedelta) field that accepts minutes (integer) as input

    Bashed together from django's 'DurationField' and 'IntegerField'. 
    Probably could be simpler, but kept all of django's checks and other methods
    in place. They're probably there for a reason...
    """
    widget = NumberInput
    re_decimal = _lazy_re_compile(r'\.0*\s*$')

    def prepare_value(self, value):
        if isinstance(value, datetime.timedelta):
            return int(value.total_seconds() / 60)
        return value

    def to_python(self, value):
        if value in self.empty_values:
            return None
        if isinstance(value, datetime.timedelta):
            return value

        if self.localize:
            value = formats.sanitize_separators(value)
        # Strip trailing decimal and zeros.
        try:
            value = int(self.re_decimal.sub('', str(value)))
            value = datetime.timedelta(minutes=value)
        except (ValueError, TypeError):
            raise ValidationError(self.error_messages['invalid'], code='invalid')
        return value