from django import forms
from django.forms.widgets import NumberInput


class RangeInput(NumberInput):
    input_type = "range"


class TagSelectMultiple(forms.CheckboxSelectMultiple):
    template_name = 'forms/widgets/tag_select_multiple.html'


class TagSelect(forms.RadioSelect):
    template_name = 'forms/widgets/tag_select.html'
