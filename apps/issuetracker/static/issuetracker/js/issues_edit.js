"use strict";

const priorityInput = document.getElementById("id_priority");
const tagNameInput = document.getElementById("id_tag_name");
const tagMessageElement = document.querySelector(
  "#new_tag_modal .form__help_text"
);
const tagAddButton = document.getElementById("add_btn");

// Calculate Color from Priority
// -----------------------------------------------------------------------------
const priorityColor = function (value) {
  let p = value / 100;
  let hue = 213 * (1 - p);
  let opacity = 0.2 * p;
  return `hsla(${hue}, 100%, 50%, ${opacity})`;
};

// Update UI
// -----------------------------------------------------------------------------
const ui_setPriorityColor = function (priority) {
  let color = priorityColor(priority);
  let el = document.getElementById("id_priority_group");
  el.style = `background: ${color}`;
};
const ui_setIssueTagMessage = function (valid) {
  if (valid) {
    tagMessageElement.innerHTML = "Valid Name";
    tagMessageElement.style["color"] = "var(--color-green)";
    tagAddButton.removeClass("hidden");
  } else {
    tagMessageElement.innerHTML = "Invalid Name";
    tagMessageElement.style["color"] = "var(--color-red)";
    tagAddButton.addClass("hidden");
  }
};

// AJAX - validate issue tag name
// -----------------------------------------------------------------------------
let checkName = (name) => {
  let body = {
    is_valid_new_name: "",
    name: tagNameInput.value,
  };
  sendHttpAsync(newTagURL, "POST", body).then((response) =>
    ui_setIssueTagMessage(response.body.is_valid)
  );
};

// AJAX - add issue tag
// -----------------------------------------------------------------------------
let addTag = (name) => {
  let body = {
    new_issue_tag: "",
    name: tagNameInput.value,
  };
  sendHttpAsync(newTagURL, "POST", body).then((response) => {
    tagNameInput.value = "";
    location.reload();
  });
};

// Event Listentters
// -----------------------------------------------------------------------------
tagNameInput.addEventListener("input", (e) => checkName(e.target.value));
tagNameInput.addEventListener("propertychange", (e) => {
  // for i.e.
  checkName(e.target.value);
});

priorityInput.addEventListener("change", (e) =>
  // after change complete, also i.e.
  ui_setPriorityColor(e.target.value)
);

priorityInput.addEventListener("input", (e) =>
  // while changing
  ui_setPriorityColor(e.target.value)
);

// Set Initial Priority Color
ui_setPriorityColor(priorityInput.value);
