"use strict";

let name_el = document.getElementById("name");
let message_el = document.getElementById("message");
let add_btn_el = document.getElementById("add_btn");

let checkName = (name) => {
  let body = {
    is_valid_new_name: "",
    name: name_el.value,
  };
  sendHttpAsync(`new.json/`, "POST", body).then((response) => {
    if (response.body.is_valid) {
      message_el.innerHTML = "Valid Name";
      message_el.className = "success help_text";
      add_btn_el.removeClass("hidden");
    } else {
      message_el.innerHTML = "Invalid Name";
      message_el.className = "error help_text";
      add_btn_el.addClass("hidden");
    }
  });
};

let addProject = (name) => {
  let body = {
    new_project: "",
    name: name_el.value,
  };
  sendHttpAsync(`new.json/`, "POST", body).then((response) => {
    name_el.value = "";
    location.reload();
  });
};

let handleInputUpdate = (event) => {
  let name = event.target.value;
  checkName(name);
};
name_el.addEventListener("input", handleInputUpdate);
name_el.addEventListener("propertychange", handleInputUpdate); // ie
