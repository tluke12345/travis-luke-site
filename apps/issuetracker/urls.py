from django.urls import path

from apps.issuetracker.views import issues
from apps.issuetracker.views import projects
from apps.issuetracker.views import issue_tags

urlpatterns = [

    # ISSUE
    path(
        'issues/search',
        issues.search,
        name="issues.search"
    ),
    path(
        # GET:          display form
        # POST:         submit form
        # template:     issues_new.html
        # form:         issuesNewForm.py
        'issues/new/',
        issues.new,
        name="issues.new",
    ),
    path(
        # GET:          (depreciated)
        # POST:'delete' deletes issue
        # POST:'action' performs action on issue
        #   'start':
        #   'pause':
        #   'reopen':
        #   'complete':
        #   'reject':
        # template:     issues_item.html (depreciated)
        'issues/<int:pk>/',
        issues.item,
        name="issues.item",
    ),
    path(
        # GET:          display form, option to delete, display history
        # POST:         submit form
        # template:     issues_edit.html
        # form:         issuesEditForm.py
        'issues/<int:pk>/edit/',
        issues.edit,
        name="issues.edit",
    ),
    # path(
    #     'issues/<int:pk>/edit/project/',
    #     issues.edit_project,
    #     name="issues.edit.project"
    # ),
    path(
        # GET:          display history view
        # template:     issues_history.html
        'issues/<int:pk>/history/',
        issues.history,
        name="issues.history",
    ),

    # projects
    path(
        'projects/new.json/',
        projects.new_json,
        name="projects.new",
    ),
    path(
        # POST:DELETE   projects#destroy (logic to delete existing issue)
        # POST:<other>  projects#<other> (logic to perform other action)
        'projects/<int:pk>/',
        projects.item,
        name="projects.item",
    ),
    path(
        'projects/<int:pk>/archive/',
        projects.archive,
        name="projects.archive",
    ),
    path(
        # GET:          projects#edit (show form to edit existing event)
        # POST:PUT      projects#replace (logic to 'put' existing event)
        # projects_edit.html, issuesEditForm.py
        'projects/<int:pk>/edit/',
        projects.edit,
        name="projects.edit",
    ),
    path(
        # GET:          projects#index (show list of all events)
        # projects_index.html
        'projects/',
        projects.index,
        name="projects.index",
    ),


    # Issue Tags
    path(
        'issue_tags/new.json/',
        issue_tags.new_json,
        name="issue_tags.new.json"
    )
]
