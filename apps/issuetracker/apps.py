from django.apps import AppConfig


class IssueTrackerConfig(AppConfig):
    name = 'apps.issuetracker'
