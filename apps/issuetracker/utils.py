from random import randint
from random import choices

from django.apps import apps


PROJECT_NAME = '# Project #'
TAG_NAMES = [f'# {i} #' for i in range(10)]


def create_test_issues(count):
    Project = apps.get_model('issuetracker', 'Project')
    IssueTag = apps.get_model('issuetracker', 'IssueTag')
    Issue = apps.get_model('issuetracker', 'Issue')
    project = Project.objects.for_name(PROJECT_NAME)
    tags = [IssueTag.objects.for_name(n) for n in TAG_NAMES]
    desc = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In eu pellentesque massa. Cras consequat est et mauris posuere dapibus. Ut tincidunt non massa nec luctus. Vestibulum sodales odio erat, fermentum bibendum enim imperdiet ut. Aliquam vitae augue orci. Praesent vitae risus vel dui posuere varius at eu neque. Ut malesuada ultricies nisl, id consectetur dui maximus eget. Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut posuere hendrerit egestas. '

    results = []
    for i in range(count):
        issue = Issue(
            name=f'test issue {i}',
            description=desc,
            priority=randint(0, 100),
            project=project,
        )
        Issue.objects.save(issue)
        issue.tags.set(choices(tags, k=3))

        results.append(issue)

    return results


def delete_test_issues():
    Project = apps.get_model('issuetracker', 'Project')
    IssueTag = apps.get_model('issuetracker', 'IssueTag')

    project = Project.objects.for_name(PROJECT_NAME)
    tags = [IssueTag.objects.for_name(n) for n in TAG_NAMES]

    removed_count = 1 + project.issues.count()
    Project.objects.delete(project)

    for tag in tags:
        IssueTag.objects.delete(tag)
        removed_count += 1

    return removed_count
