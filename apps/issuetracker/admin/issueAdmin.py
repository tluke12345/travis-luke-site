from django.contrib import admin
from apps.issuetracker.models import Issue
from apps.issuetracker.models import IssueUpdate


class IssueUpdateInline(admin.TabularInline):
    model = IssueUpdate
    min_num = 0
    extra = 0
    fields = [
        'name',
    ]
    show_change_link = True


@admin.register(Issue)
class IssueAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'closed_at',
        'priority',
    ]
    inlines = [
        IssueUpdateInline,
    ]
    list_filter = ('closed_at', 'tags',)
