from django.contrib import admin
from apps.issuetracker.models import IssueUpdate


@admin.register(IssueUpdate)
class IssueUpdateAdmin(admin.ModelAdmin):
    pass
