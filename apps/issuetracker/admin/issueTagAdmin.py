from django.apps import apps
from django.contrib import admin
from apps.issuetracker.models import IssueTag


@admin.register(IssueTag)
class IssueTagAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'used_count',
    ]

    def used_count(self, obj):
        Issue = apps.get_model('issuetracker', 'Issue')
        return Issue.objects.filter(tags=obj).count()
