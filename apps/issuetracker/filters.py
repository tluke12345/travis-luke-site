from collections import namedtuple
from django.db.models.query import QuerySet
import django_filters
from django_filters import fields
from django_filters import filters

from apps.issuetracker.models import Issue
from apps.issuetracker.models import Project
from apps.issuetracker.models import IssueTag


class IssueFilter(django_filters.FilterSet):

    order = filters.OrderingFilter(
        fields=(
            ('closed_at', 'Closed Date'),
            ('created_at', 'Creation Date'),
            ('priority', 'Priority'),
            ('name', 'Name'),
            ('project', 'Project'),
        )
    )
    active = filters.BooleanFilter(
        'closed_at',
        'isnull',
        label="Active",
    )
    project = filters.ModelChoiceFilter(queryset=Project.objects.all())
    priority = filters.NumericRangeFilter()

    class Meta:
        model = Issue
        fields = ['active', 'project', 'priority', 'order']
