# Generated by Django 3.1.6 on 2021-06-08 05:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('issuetracker', '0006_issueupdate_name'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='issueupdate',
            options={'ordering': ('-created_at',)},
        ),
        migrations.AlterField(
            model_name='issue',
            name='status',
            field=models.CharField(choices=[('PENDING', 'pending'), ('IN_PROGRESS', 'in progress'), ('CLOSED', 'closed')], default='PENDING', max_length=100),
        ),
    ]
