import json

from django.core.validators import MinValueValidator
from django.core.validators import MaxValueValidator
from django.db import models
from django.utils import timezone

from .issue__manager import IssueManager


class Issue(models.Model):
    created_at = models.DateTimeField(default=timezone.now)
    closed_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    priority = models.IntegerField(
        default=50,
        validators=[MinValueValidator(0), MaxValueValidator(100)]
    )
    project = models.ForeignKey(
        "Project",
        related_name='issues',
        on_delete=models.CASCADE,
    )
    tags = models.ManyToManyField(
        'IssueTag',
        related_name='+',
        blank=True,
    )

    class Meta:
        ordering = ('-priority', 'created_at')

    objects = IssueManager()

    def __str__(self):
        return self.name

    # def get_absolute_url(self):
    #     return reverse("_detail", kwargs={"pk": self.pk})

    @property
    def priority_color(self):
        p = self.priority / 100
        hue = 213 * (1 - p)
        opacity = .2 * p
        return f'hsla({hue}, 100%, 50%, {opacity})'

    def json(self):
        return json.dumps({
            'name': self.name,
            # 'created_at': self.created_at,
            # 'closed_at': self.closed_at,
            'priority': self.priority,
            'project': self.project.name,
            'tags': [t.name for t in self.tags.all()]
        })
