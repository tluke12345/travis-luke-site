from django.db import models

from .issue_tag__queryset import IssueTagQuerySet


class IssueTagManager(models.Manager):
    def get_queryset(self):
        return IssueTagQuerySet(self.model, using=self._db)

    def for_name(self, name):
        tag, is_new = self.get_queryset().get_or_create(name=name)
        return tag

    def safe_get(self, pk):
        try:
            return self.model.objects.get(pk=pk)
        except self.model.DoesNotExist:
            return None

    def delete(self, tag):
        tag.delete()

    def is_valid_new_name(self, name):
        if not name:
            return False
        if self.get_queryset().filter(name=name).exists():
            return False
        return True
