from django.db import models
from django.db.models import Q


class IssueQuerySet(models.QuerySet):

    def project(self, project):
        return self.filter(project=project)

    def active(self):
        return self.filter(closed_at__isnull=True)

    def closed(self):
        return self.filter(closed_at__isnull=False)

    def matching_query(self, query):
        return self.filter(
            Q(name__icontains=query) |
            Q(description__icontains=query) |
            Q(tags__name__icontains=query)
        ).distinct()

    def order_by_closed_at_reverse(self):
        return self.order_by('-closed_at')

    def order_new_to_old(self):
        return self.order_by('-created_at')
