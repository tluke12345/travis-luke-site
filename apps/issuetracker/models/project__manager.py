from django.db import models

from .project__queryset import ProjectQuerySet


class ProjectManager(models.Manager):
    def get_queryset(self):
        return ProjectQuerySet(self.model, using=self._db)

    def save(self, project):
        project.save()

    def update(self, project):
        project.save()

    def delete(self, project):
        project.delete()

    def for_name(self, name):
        if not name:
            return None
        project, is_new = self.get_queryset().get_or_create(name=name)
        return project

    def is_valid_new_name(self, name):
        if not name:
            return False
        if self.get_queryset().filter(name=name).exists():
            return False
        return True

    def get_by_pk(self, pk):
        if not pk:
            return None
        return self.get_queryset().filter(pk=pk).first()
