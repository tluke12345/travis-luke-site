from django.db import models
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger


class IssueTagQuerySet(models.QuerySet):

    def paginated(self, page, per_page=10):
        paginator = Paginator(self, per_page)
        try:
            return paginator.page(page)
        except PageNotAnInteger:
            return paginator.page(1)
        except EmptyPage:
            return paginator.page(paginator.num_pages)