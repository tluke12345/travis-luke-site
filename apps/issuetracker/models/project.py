import json

from django.apps import apps
from django.db import models
from django.utils.text import slugify

from .project__manager import ProjectManager


class Project(models.Model):

    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, unique=True, blank=True)

    objects = ProjectManager()

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(Project, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def json(self):
        return json.dumps({
            'name': self.name,
            'slug': self.slug,
        })

    def active_issue_count(self):
        Issue = apps.get_model('issuetracker', 'Issue')
        return Issue.objects.active_for_project(self).count()
