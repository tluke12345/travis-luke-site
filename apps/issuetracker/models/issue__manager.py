from django.apps import apps
from django.db import models
from django.utils import timezone

from apps.issuetracker.utils import create_test_issues as create_test
from apps.issuetracker.utils import delete_test_issues as delete_test

from .issue__queryset import IssueQuerySet


def get_IssueUpdate():
    return apps.get_model('issuetracker', 'IssueUpdate')


class IssueManager(models.Manager):
    def get_queryset(self):
        return IssueQuerySet(self.model, using=self._db)

    def save(self, issue):
        issue.save()

    def delete(self, issue):
        issue.delete()

    def active_for_project(self, project):
        return self.get_queryset().project(project).active()

    def closed_for_project(self, project):
        return self.get_queryset() \
            .project(project) \
            .closed() \
            .order_by_closed_at_reverse()

    def matching_query(self, query):
        issues = self.get_queryset().order_new_to_old()
        if query:
            return issues.matching_query(query)
        else:
            return issues

    # --------------------------------------------------------------------------
    # ACTIONS
    # --------------------------------------------------------------------------
    def reopen_action(self, issue):
        issue.closed_at = None
        self.save(issue)

        msg = 'Reopen'
        IssueUpdate = get_IssueUpdate()
        update = IssueUpdate(name=msg, issue=issue)
        IssueUpdate.objects.save(update)

    def close_action(self, issue, msg):
        issue.closed_at = timezone.now()
        self.save(issue)

        IssueUpdate = get_IssueUpdate()
        update = IssueUpdate(name=msg, issue=issue)
        IssueUpdate.objects.save(update)

    def update_action(self, issue, message='', notes=''):
        IssueUpdate = get_IssueUpdate()
        update = IssueUpdate(name=message, notes=notes, issue=issue)
        IssueUpdate.objects.save(update)

    def set_project(self, issue, project_pk):
        Project = apps.get_model('issuetracker', 'Project')
        project = Project.objects.get_by_pk(project_pk)
        issue.project = project
        self.save(issue)
    # --------------------------------------------------------------------------
    # TESTING
    # --------------------------------------------------------------------------

    def create_test_issues(self, count):
        return create_test(count)

    def delete_test_issues(self):
        return delete_test()
