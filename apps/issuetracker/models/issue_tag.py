import json

from django.db import models
from django.utils.text import slugify

from .issue_tag__manager import IssueTagManager


class IssueTag(models.Model):
    name = models.CharField(max_length=255, unique=True, db_index=True)
    slug = models.SlugField(max_length=255, unique=True, blank=True)

    objects = IssueTagManager()

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)
        super(IssueTag, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    def json(self):
        return json.dumps({
            'name': self.name,
            'slug': self.slug,
        })
