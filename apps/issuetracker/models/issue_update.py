from django.db import models
from django.utils import timezone

from .issue_update__manager import IssueUpdateManager


class IssueUpdate(models.Model):
    created_at = models.DateTimeField(default=timezone.now)
    name = models.CharField(max_length=500, blank=True)
    notes = models.TextField(blank=True)

    issue = models.ForeignKey(
        "Issue",
        related_name='updates',
        on_delete=models.CASCADE,
    )

    class Meta:
        ordering = ('created_at',)

    objects = IssueUpdateManager()

    def __str__(self):
        return f'{self.created_at: %Y/%m/%d %H:%M}: {self.name}'
