from django.db import models

from apps.issuetracker.models import Issue

# from .issue__queryset import IssueQuerySet


class IssueUpdateManager(models.Manager):
    # def get_queryset(self):
    # return IssueQuerySet(self.model, using=self._db)

    def save(self, update):
        update.save()
