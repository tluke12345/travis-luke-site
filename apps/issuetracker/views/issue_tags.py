import json

from django.contrib.auth.decorators import user_passes_test
from django.http import JsonResponse
from django.http import Http404

from apps.issuetracker.models import IssueTag
from apps.issuetracker.decorators import is_issuetracker_user


@user_passes_test(is_issuetracker_user)
def new_json(request):

    if request.method == 'POST':
        data = json.loads(request.body)

        if 'is_valid_new_name' in data:
            name = data.get('name')
            is_valid = IssueTag.objects.is_valid_new_name(name)
            return JsonResponse({'is_valid': is_valid})

        if 'new_issue_tag' in data:
            name = data.get('name')
            item = IssueTag.objects.for_name(name)
            return JsonResponse({'issue_tag': item.json()})

    raise Http404("Invalid request")
