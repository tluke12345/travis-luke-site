from django.contrib.auth.decorators import user_passes_test
from django.http import Http404
from django.http import QueryDict
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import reverse

from apps.core.http import htmx_redirect
from apps.core.utils import paginate

from apps.issuetracker.models import Issue
from apps.issuetracker.models import IssueTag
from apps.issuetracker.models import Project
from apps.issuetracker.decorators import is_issuetracker_user
from apps.issuetracker.forms.issuesForm import IssuesForm
from apps.issuetracker.forms.tagForm import TagForm
from apps.issuetracker.filters import IssueFilter


@ user_passes_test(is_issuetracker_user)
def search(request):
    if request.method == "GET":
        template_name = 'issuetracker/issues_search.html'
        get = request.GET.copy()

        query = None
        if 'query' in get:
            query = get.pop('query')[0]
            template_name = 'issuetracker/includes/issues_page.html'

        page = None
        if 'page' in get:
            page = int(get.pop('page')[0])
            template_name = 'issuetracker/includes/issues_page.html'

        f = IssueFilter(get, Issue.objects.matching_query(query))
        context = {
            'issues': paginate(f.qs, page, items_per_page=20),
            'filter': f
        }
        return render(request, template_name, context)


@ user_passes_test(is_issuetracker_user)
def new(request):
    template_name = 'issuetracker/issues_new.html'
    project = Project.objects.for_name(request.GET.get('project'))

    if request.method == 'GET':
        form = IssuesForm(initial={'project': project})
    elif request.method == 'POST':
        form = IssuesForm(request.POST, initial={'project': project})
        if form.is_valid():
            issue = form.save(commit=False)
            Issue.objects.save(issue)
            form.save_m2m()

            url = reverse('projects.item', args=[project.pk])
            return redirect(url)

    context = {
        'form': form,
        'tag_form': TagForm(),
    }
    return render(request, template_name, context)


@ user_passes_test(is_issuetracker_user)
def item(request, pk):
    issue = get_object_or_404(Issue, pk=pk)
    referer = reverse('projects.item', args=[issue.project.pk])

    # HTMX
    if request.method == "DELETE":
        Issue.objects.delete(issue)
        return htmx_redirect(referer)

    if request.method == 'POST':
        if request.POST.get('action') == 'reopen':
            Issue.objects.reopen_action(issue)

        elif request.POST.get('action') == 'complete':
            Issue.objects.close_action(issue, 'Complete')

        elif request.POST.get('action') == 'reject':
            Issue.objects.close_action(issue, 'Rejected')

        return redirect(referer)

    raise Http404("Invalid request")


@ user_passes_test(is_issuetracker_user)
def edit(request, pk):
    template_name = 'issuetracker/issues_edit.html'
    issue = get_object_or_404(Issue, pk=pk)
    referer = reverse('projects.item', args=[issue.project.pk])

    if request.method == 'GET':
        form = IssuesForm(instance=issue)

    if request.method == 'POST':

        # used to determine types of changes
        initial = {
            'priority': issue.priority,
            'name': issue.name,
            'description': issue.description,
            'tags': [t.pk for t in issue.tags.all()],
            'project': issue.project.pk,
        }

        form = IssuesForm(request.POST, instance=issue)
        if form.is_valid():
            issue = form.save(commit=False)

            # determine changes
            if issue.priority != initial['priority']:
                msg = "Update Priority"
                notes = f"{initial['priority']} -> {issue.priority}"
                Issue.objects.update_action(issue, message=msg, notes=notes)
            if issue.name != initial['name']:
                msg = "Update Name"
                notes = f"{initial['name']} -> {issue.name}"
                Issue.objects.update_action(issue, message=msg, notes=notes)
            if issue.description != initial['description']:
                msg = "Update Description"
                Issue.objects.update_action(issue, message=msg)
            if issue.project.pk != initial['project']:
                msg = "Update Project"
                Issue.objects.update_action(issue, message=msg)

            # Save
            Issue.objects.save(issue)
            form.save_m2m()

            # Detect Tag Changes
            # This step must happend after the save_m2m method
            new_tags = [t.pk for t in issue.tags.all()]
            old_tags = initial['tags']

            for new_tag in new_tags:
                if new_tag not in old_tags:
                    msg = "Add Tag"
                    msg = IssueTag.objects.safe_get(new_tag).name
                    Issue.objects.update_action(issue, message=msg)

            for old_tag in old_tags:
                if old_tag not in new_tags:
                    msg = 'Remove Tag'
                    notes = IssueTag.objects.safe_get(old_tag).name
                    Issue.objects.update_action(issue, message=msg)

            # Other Actions
            if request.POST.get('action') == 'save_complete':
                Issue.objects.close_action(issue, 'Complete')
            elif request.POST.get('action') == 'save_reject':
                Issue.objects.close_action(issue, 'Reject')
            elif request.POST.get('action') == 'save_reopen':
                Issue.objects.reopen_action(issue)

            return redirect(referer)

    context = {
        'issue': issue,
        'form': form,
        'tag_form': TagForm(),
    }
    return render(request, template_name, context)


@user_passes_test(is_issuetracker_user)
def history(request, pk):
    template_name = 'issuetracker/issues_history.html'
    issue = get_object_or_404(Issue, pk=pk)
    context = {
        'issue': issue,
    }
    return render(request, template_name, context)
