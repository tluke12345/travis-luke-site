import json

from django.contrib.auth.decorators import user_passes_test
from django.db import close_old_connections
from django.http import JsonResponse
from django.http import Http404
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.shortcuts import reverse

from apps.core.http import htmx_redirect
from apps.core.utils import paginate

from apps.issuetracker.models import Project
from apps.issuetracker.models import Issue
from apps.issuetracker.decorators import is_issuetracker_user
from apps.issuetracker.forms.projectsForm import ProjectsForm

APP_NAME = 'issuetracker'
MODEL_PLURAL_NAME = 'projects'
MODEL = Project
EDIT_FORM = ProjectsForm


@user_passes_test(is_issuetracker_user)
def index(request):
    template_name = f'{APP_NAME}/{MODEL_PLURAL_NAME}_index.html'
    query = request.GET.copy()
    page = 1
    if 'page' in query:
        page = int(query.pop('page')[0])
    items = MODEL.objects.all()
    context = {
        'items': items.paginated(page),
        'query_str': query.urlencode(),
    }
    return render(request, template_name, context)


@user_passes_test(is_issuetracker_user)
def new_json(request):

    if request.method == 'POST':
        data = json.loads(request.body)

        if 'is_valid_new_name' in data:
            name = data.get('name')
            is_valid = MODEL.objects.is_valid_new_name(name)
            return JsonResponse({'is_valid': is_valid})

        if 'new_project' in data:
            name = data.get('name')
            item = MODEL.objects.for_name(name)
            return JsonResponse({'project': item.json()})

    raise Http404("Invalid request")


@user_passes_test(is_issuetracker_user)
def item(request, pk):
    template_name = f'{APP_NAME}/{MODEL_PLURAL_NAME}_item.html'
    item = get_object_or_404(MODEL, pk=pk)

    issues = Issue.objects.active_for_project(item)

    if request.method == 'DELETE':
        MODEL.objects.delete(item)
        return htmx_redirect(reverse('projects.index'))

    context = {
        'project': item,
        'issues': paginate(issues, request.GET.get('page', 1), 20),
    }
    return render(request, template_name, context)


@user_passes_test(is_issuetracker_user)
def archive(request, pk):
    template_name = f'issuetracker/projects_archive.html'
    project = get_object_or_404(Project, pk=pk)

    issues = Issue.objects.closed_for_project(project)

    context = {
        'project': project,
        'issues': paginate(issues, request.GET.get('page', 1), 20),
    }
    return render(request, template_name, context)


@user_passes_test(is_issuetracker_user)
def edit(request, pk):
    template_name = f'{APP_NAME}/{MODEL_PLURAL_NAME}_edit.html'
    item = get_object_or_404(MODEL, pk=pk)
    if request.method == 'GET':
        form = ProjectsForm(instance=item)
    elif request.method == 'POST':
        form = ProjectsForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save(commit=False)
            MODEL.objects.save(item)
            if form.save_m2m:
                form.save_m2m()
            url = reverse(f'{MODEL_PLURAL_NAME}.item', args=[item.pk])
            return redirect(url)

    context = {
        'item': item,
        'form': form,
    }
    return render(request, template_name, context)
