from django import forms

from apps.forms.widgets import RangeInput
from apps.forms.widgets import TagSelectMultiple
from apps.issuetracker.models import Issue


class IssuesForm(forms.ModelForm):

    class Meta:
        model = Issue
        fields = [
            'name',
            'description',
            'priority',
            'tags',
            'project',
        ]
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Name', 'maxlength': 100}),
            'description': forms.Textarea(attrs={'cols': 30, 'rows': 8, 'placeholder': 'Description'}),
            'priority': RangeInput(),
            'tags': TagSelectMultiple(),
        }
