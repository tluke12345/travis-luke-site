from django import forms

from apps.issuetracker.models import Project


class ProjectsForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = [
            'name',
        ]
        widgets = {}
        labels = {}
