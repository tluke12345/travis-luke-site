from django import template

from apps.core.utils import time_until as tu

register = template.Library()


@register.filter
def time_until(dt):
    return tu(dt)
