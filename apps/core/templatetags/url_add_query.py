from urllib.parse import urlsplit, urlunsplit
from django import template
from django.http import QueryDict


register = template.Library()

@register.simple_tag
def url_add_query(url, replace=False, **kwargs):
    """
    Lets you append a querystring to a url.

    The querystring argument is appended to the current querystring.
    Absolute and relative urls OK


    args:
        url: current url whose querystring you want to modify
        replace: if key already exists in querystring, replace it with new value

    Modified version of:
        leonsmith
        https://gist.github.com/leonsmith/5590500
        7/2/2021

    Usage:
        {% load url_add_query %}
        <a href="{% url_add_query request.get_full_path overwrite=True page=2 %}">link</a>

    Example: 

        BEFORE:
        > url -> example.com/?query=test&page=1
        
        AFTER (replace=True) (NOTE new 'page' key):
        > url -> example.com/?query=test&page=2
        
        AFTER (replace=False) (NOTE duplicate 'page' keys):
        > url -> example.com/?query=test&page=1&page=2

    """

    parsed = urlsplit(url)
    querystring = QueryDict(parsed.query, mutable=True)
    for k, v in kwargs.items():
        
        # v is Truthy OR v == 0 or 0.0 
        if v or isinstance(v, (int, float)):

            if replace:
                # sets k = ['v'] removing any previous value(s)
                querystring[k] = v
            else:
                querystring.appendlist(k, v)

        
        # v is Falsy (don't set new value) AND has existing value for k
        elif k in querystring:

            # removes k entirely from the queryset
            del querystring[k]

    return urlunsplit(parsed._replace(query=querystring.urlencode()))