from django.test import TestCase
from apps.core.templatetags.url_add_query import url_add_query


class URLAddQueryTestCase(TestCase):
    # def setUp(self):
    #     Animal.objects.create(name="lion", sound="roar")
    #     Animal.objects.create(name="cat", sound="meow")

    # def test_animals_can_speak(self):
    # """Animals that can speak are correctly identified"""
    #     lion = Animal.objects.get(name="lion")
    #     cat = Animal.objects.get(name="cat")
    #     self.assertEqual(lion.speak(), 'The lion says "roar"')
    #     self.assertEqual(cat.speak(), 'The cat says "meow"')

    def test_absolute_url(self):
        url = 'https://www.example.com/example/1?test=True'
        expect = 'https://www.example.com/example/1?test=True&page=1'
        self.assertEqual(url_add_query(url, page=1), expect)

    def test_relative_url(self):
        url = '/example/1?test=True'
        expect = '/example/1?test=True&page=1'
        self.assertEqual(url_add_query(url, page=1), expect)

    def test_add_query(self):
        # add many
        url = '/example/1?test=True'
        expect = '/example/1?test=True&page=1&order=asc'
        self.assertEqual(url_add_query(url, page=1, order='asc'), expect)
        # add one to existing
        url = '/example/1?page=1'
        expect = '/example/1?page=1&page=2'
        self.assertEqual(url_add_query(url, page=2), expect)

    def test_replace(self):
        # replace one
        url = '/example/1?page=1'
        expect = '/example/1?page=2'
        self.assertEqual(url_add_query(url, replace=True, page=2), expect)

    def test_remove(self):
        # remove one
        url = '/example/1?page=1'
        expect = '/example/1'
        self.assertEqual(url_add_query(url, replace=True, page=None), expect)
        # remove many
        url = '/example/1?page=1&test=True&name=john'
        expect = '/example/1?page=1'
        self.assertEqual(url_add_query(
            url, replace=True, test=None, name=None), expect)
        # no change if key to remove doesn't already exist
        url = '/example/1?page=1'
        expect = '/example/1?page=1'
        self.assertEqual(url_add_query(url, replace=True, name=None), expect)
        # special case: ignore int 0
        url = '/example/1?page=1'
        expect = '/example/1?page=0'
        self.assertEqual(url_add_query(url, replace=True, page=0), expect)
        # special case: ignore float 0.0
        url = '/example/1?page=1'
        expect = '/example/1?page=0.0'
        self.assertEqual(url_add_query(url, replace=True, page=0.0), expect)
