from django.http import HttpResponse, response


def htmx_redirect(url):
    response = HttpResponse()
    response['HX-Redirect'] = url
    return response


def htmx_refresh():
    response = HttpResponse()
    # response['HX-Refresh'] = True
    response['HX-Trigger'] = 'htmxRefreshAction'
    return response
