from datetime import datetime
from typing import Optional

from django.utils import timezone


def suffix(d: int) -> str:
    return 'th' if 11 <= d <= 13 else {1: 'st', 2: 'nd', 3: 'rd'}.get(d % 10, 'th')


def time_until(dt: datetime, current: Optional[datetime] = None) -> str:
    dt = timezone.localtime(dt)
    now: datetime = current or timezone.now()

    if not now:
        now = timezone.now()

    # is Past
    if dt < now:
        return 'past'

    # is Today
    if dt.date() == now.date():
        return f'today, {dt: %H:%M}'

    # is Current Week
    # previously used .isocalendar() but it gave incorrect results
    now_year = now.year
    now_week = int(now.strftime("%U"))
    dt_year = dt.year
    dt_week = int(dt.strftime("%U"))

    if now_year == dt_year and now_week == dt_week:
        # %a for short day of week
        return f'{dt: %a}, {dt: %H:%M}'

    # is Next Week
    if now_year == dt_year and dt_week == now_week + 1:
        return f'next week, {dt: %a}'

    # is Current Month
    now_month = now.month
    dt_month = dt.month
    day = str(dt.day) + suffix(dt.day)
    if now_year == dt_year and dt_month == now_month:
        return f'the {day}, {dt: %H:%M}'

    # is Next Month
    if now_year == dt_year and dt_month == now_month + 1:
        return 'next month, the ' + day

    # is This Year
    if now_year == dt_year:
        # %B for full month
        return f'this year, {dt: %b}'

    # is Next Year
    if dt_year == now_year + 1:
        return f'next year, {dt: %b}'

    # Future
    return f'{dt: %B} {dt_year}'
