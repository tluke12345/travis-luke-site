from django.contrib import admin


from apps.email.models import EmailReceipt


@admin.register(EmailReceipt)
class EmailReciptAdmin(admin.ModelAdmin):
    list_display = [
        'recipient',
        'email_type',
        'status',
    ]
    list_filter = ('email_type',)

    def status(self, obj):
        code = obj.status_code or '---'
        if obj.is_success:
            msg = 'success'
        else:
            msg = 'fail'
        return f'{code} ({msg})'
