import os
from django.template.loader import get_template
from django.utils.html import strip_tags


def render_template(template_name, context):
    return get_template(template_name).render(context)


def text_from_html(html):
    return strip_tags(html)


def get_email_address(env_var_name):
    return os.environ.get(env_var_name)
