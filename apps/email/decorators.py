def is_email_user(user):
    """
    Validate user is part of the 'email' group
    Use with django.contrib.auth.decorators.user_passes_test function:

        from django.contrib.auth.decorators import user_passes_test

        @user_passes_test(is_email_user)
        def view(request):
            ...
    """
    if user.is_superuser:
        return True
    else:
        return user.groups.filter(name="email").exists()