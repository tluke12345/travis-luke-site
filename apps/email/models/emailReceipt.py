from django.apps import apps
from django.db import models
from django.utils import timezone

from .emailReceipt_manager import EmailReceiptManager


class EmailReceipt(models.Model):
    timestamp = models.DateTimeField(default=timezone.now)

    # Email Details
    sender = models.EmailField(max_length=254)
    recipient = models.EmailField(max_length=254)
    status_code = models.IntegerField(null=True, blank=True)
    details = models.CharField(max_length=500, blank=True)

    # META
    email_type = models.CharField(max_length=200, blank=True)
    related_type = models.CharField(max_length=50, blank=True)
    related_id = models.PositiveIntegerField(null=True, blank=True)

    objects = EmailReceiptManager()

    class Meta:
        ordering = ('-timestamp',)

    def __str__(self):
        dt = timezone.localtime(self.timestamp)
        time = f'{dt: %Y/%m/%d %H:%M}'
        return f'{time}: -> {self.recipient}'

    @property
    def is_success(self):
        return self.status_code == 201

    @property
    def reservation(self):
        if self.related_type == 'Reservation' and self.related_id:
            Reservation = apps.get_model(
                app_label='event', model_name="Reservation")
            return Reservation.objects.get_by_pk(self.related_id)

    @property
    def message(self):
        if self.related_type == 'Message' and self.related_id:
            Message = apps.get_model(
                app_label='message', model_name="Message")
            return Message.objects.get_by_pk(self.related_id)
