from django.db import models

from .emailReceipt_queryset import EmailReceiptQuerySet


class EmailReceiptManager(models.Manager):
    def get_queryset(self):
        return EmailReceiptQuerySet(self.model, using=self._db)

    def customer_receipts_for_reservation(self, reservation):
        return self.get_queryset() \
            .for_reservation(reservation) \
            .to_recipient(reservation.email) \
            .chronological()

    def reservation_has_receipt_of_type(self, reservation, email_type):
        return self.get_queryset() \
            .for_reservation(reservation) \
            .filter(email_type=email_type) \
            .count() > 0