from django.db import models
from django.core.paginator import Paginator
from django.core.paginator import EmptyPage
from django.core.paginator import PageNotAnInteger


class EmailReceiptQuerySet(models.QuerySet):
    def for_reservation(self, reservation):
        return self \
            .filter(related_type="Reservation") \
            .filter(related_id=reservation.pk)

    def to_recipient(self, recipient):
        return self.filter(recipient=recipient)

    def chronological(self):
        return self.order_by('timestamp')