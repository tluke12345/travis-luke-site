import requests
import os

from apps.email.models import EmailReceipt


def send(
    sender,
    sender_name,
    recipient,
    subject,
    html,
    text,
    reply_to=None,
    commit=True,
    test_mode=False,
):
    if not sender_name:
        sender_name = 'Travis Luke'

    receipt = EmailReceipt(
        sender=f'{sender_name} <{sender}>',
        recipient=recipient
    )
    details = ['Backend: SendInBlue']

    # Do not send if Emails are disabled in Environment Variables
    email_disabled = os.environ.get('DISABLE_EMAIL')
    if email_disabled and email_disabled != 'FALSE':
        receipt.details = 'Email disabled in environment variables'
        if commit == True:
            receipt.save()
        return receipt

    url = os.environ.get('SENDINBLUE_BASE_URL') + '/smtp/email'
    payload = {
        "sender": {
            "name": sender_name,
            "email": sender,
        },
        "to": [{"email": recipient}],
        "tags": ["reservation.confirmation"],
        "htmlContent": html,
        "textContent": text,
        "subject": subject,
    }
    if reply_to:
        payload["replyTo"] = {'email': reply_to}
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Api-Key": os.environ.get('SENDINBLUE_API_KEY'),
    }
    try:

        # Test Mode
        test_mode = test_mode or os.environ.get('EMAIL_TEST_MODE')
        if test_mode and test_mode != 'FALSE':
            details.append('TEST MODE')
            receipt.status_code = '201'

        # SEND
        else:
            response = requests.request(
                "POST", url, json=payload, headers=headers)

            details.append(f'MessageId:')
            details.append(f'  {response.json().get("messageId", "??")}')
            receipt.status_code = response.status_code

    except Exception as e:
        details.append(f'Response Error:\n\t{str(e)}')

    finally:
        receipt.details = '\n'.join(details)
        if commit == True:
            receipt.save()
        return receipt
