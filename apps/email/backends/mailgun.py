import os
import requests

from apps.email.models import EmailReceipt


def send(
    sender,
    sender_name,
    recipient,
    subject,
    html,
    text,
    reply_to=None,
    commit=True,
    test_mode=False,
):
    # sender can be in form: Travis Luke <support@mg.travis-luke.com>
    if sender_name:
        sender = f'{sender_name} <{sender}>'

    receipt = EmailReceipt(sender=sender, recipient=recipient)
    details = ['Backend: Mailgun']

    # Do not send if Emails are disabled in Environment Variables
    email_disabled = os.environ.get('DISABLE_EMAIL')
    if email_disabled and email_disabled != 'FALSE':
        receipt.details = 'Email disabled in environment variables'
        if commit == True:
            receipt.save()
        return receipt

    try:
        url = os.environ.get('MAILGUN_BASE_URL') + '/messages'
        auth = ('api', os.environ.get('MAILGUN_API_KEY'))
        data = {
            'from': sender,
            'to': [recipient],
            'subject': subject,
            'text': text,
            'html': html,
        }
        if reply_to:
            data['h:Reply-To'] = reply_to

        # Set test mode
        test_mode = test_mode or os.environ.get('MAILGUN_TEST_MODE')
        if test_mode and test_mode != 'FALSE':
            data['o:testmode'] = True
            details.append('TEST MODE')

        response = requests.post(url, auth=auth, data=data)

        details.append(f'Response ID:\n {response.json().get("id", "??")}')
        details.append(f'Message:\n {response.json().get("message", "??")}')
        receipt.status_code = response.status_code

    except Exception as e: details.append(f'Response Error:\n\t{str(e)}')

    finally:
        receipt.details = '\n'.join(details)
        if commit == True:
            receipt.save()
        return receipt
