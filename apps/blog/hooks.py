from django.urls import reverse

from wagtail.core import hooks
from wagtail.admin.menu import MenuItem


@hooks.register('register_admin_menu_item')
def register_manager_menu_item():
    return MenuItem('Manager', reverse('m.index'), classnames='icon icon-home', order=0)
