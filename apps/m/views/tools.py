from django.contrib import messages as notify
from django.shortcuts import render
from django.core.management import call_command

# from apps.eventManager import email as m_email

from apps.message.models import Message
from apps.issuetracker.models import Issue
from apps.issuetracker.models import Project
from apps.issuetracker.models import IssueTag
from apps.event.models import Location
from apps.event.models import Reservation
from apps.hourtracker.models import Category as HoursCategory
from apps.hourtracker.models import Entry as HoursEntry


def index(request):
    template_name = "m/tools.html"
    if request.method == 'POST':

        # RESERVATIONS
        if request.POST.get('reservations') == 'create':
            count = Reservation.objects.create_test_reservations(20)
            notify.success(request, f'created {count} test reservations')
        if request.POST.get('reservations') == 'delete':
            count = Reservation.objects.delete_test_reservations()
            notify.error(request, f'deleted {count} test reservations')

        # MESSAGES
        if request.POST.get('messages') == 'create':
            count = Message.objects.create_test_messages(20)
            notify.success(request, f'created {count} test messages')
        if request.POST.get('messages') == 'delete':
            count = Message.objects.delete_test_messages()
            notify.error(request, f'deleted {count} test messages')

        # Locations
        if request.POST.get('locations') == 'create':
            count = Location.objects.create_test_items(20)
            notify.success(request, f'created {count} test messages')
        if request.POST.get('locations') == 'delete':
            count = Location.objects.delete_test_items()
            notify.error(request, f'deleted {count} test messages')

        if 'issues' in request.POST:
            action = request.POST.get('issues')
            if action == 'create':
                count = Issue.objects.create_test_issues(20)
                notify.success(request, f'created {count} items')
            if action == 'delete':
                count = Issue.objects.delete_test_issues()
                notify.error(request, f'deleted {count} items')

        # Hours Tracker
        if request.POST.get('hour_tracker') == 'load':
            call_command('loaddata', 'fixtures/hourtracker.json')
        if request.POST.get('hour_tracker') == 'reset':
            for category in HoursCategory.objects.all():
                category.delete()

        # if 'email' in request.POST:
        #     value = request.POST.get('email')
        #     success = None
        #     if value == 'manager.reservation.received':
        #         reservation = Reservation.objects.example()
        #         success = m_email.send_reservation_received(reservation)
        #     if value == 'manager.reservation.canceled':
        #         reservation = Reservation.objects.example()
        #         success = m_email.send_reservation_canceled(reservation)
        #     if value == 'manager.reservation.moved':
        #         reservation = Reservation.objects.example()
        #         success = m_email.send_reservation_moved(reservation)
        #     if value == 'manager.message.received':
        #         message = Message.objects.example()
        #         success = m_email.send_message_received(message)

        #     if success != None and success:
        #         notify.success(request, f'Successfully sent {value} email')
        #     else:
        #         notify.error(request, f'Error sending {value} email')

    context = {
        'message_count': Message.objects.count(),
        'issue_count': Issue.objects.count(),
        'issue_tag_count': IssueTag.objects.count(),
        'project_count': Project.objects.count(),
        # 'default_email_to': 'tlukejp@gmail.com',
        'location_count': Location.objects.count(),
        'reservations_count': Reservation.objects.count(),
        'hour_tracker': {
            'categories': HoursCategory.objects.count(),
            'entries': HoursEntry.objects.count(),
        }
    }
    return render(request, template_name, context)
