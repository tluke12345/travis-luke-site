from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import redirect
from django.shortcuts import render

from apps.calendar import utils
from apps.event.models import Event
from apps.calendar.models import NamedDay


from apps.eventManager.decorators import is_manager


@user_passes_test(is_manager)
def index(request):
    template_name = 'm/index.html'
    context = {
    }
    return render(request, template_name, context)
