from django.urls import path

from apps.m.views import main
from apps.m.views import tools

urlpatterns = [

    path(
        '',
        main.index,
        name="m.index"
    ),
    
    
    # TOOLS
    path(
        'tools/',
        tools.index,
        name="m.tools"
    ),
]