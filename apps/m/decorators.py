# from functools import wraps
# from django.core.exceptions import PermissionDenied
# def manager(view_func):
#     @wraps(view_func)
#     def _wrapped_view(request, *args, **kwargs):
#         if not is_manager(request.user):
#             raise PermissionDenied()

#         return view_func(request, *args, **kwargs)
#     return _wrapped_view


def is_manager(user):
    """
    Validate user is part of the 'manager' group
    Use with django.contrib.auth.decorators.user_passes_test function:

        from django.contrib.auth.decorators import user_passes_test

        @user_passes_test(is_manager)
        def view(request):
            ...
    """
    if user.is_superuser:
        return True
    else:
        return user.groups.filter(name="manager").exists()
