from django.urls import path
from django.views.generic import TemplateView

from apps.portfolio import views

urlpatterns = [

    path(
        '',
        TemplateView.as_view(template_name="portfolio/portfolio.html"),
        name="portfolio"
    ),
    path(
        'cv/',
        TemplateView.as_view(template_name="portfolio/cv.html"),
        name="portfolio.cv"
    ),
    path(
        'project/<int:number>/',
        views.project,
        name="portfolio.project"
    ),
    path(
        'contact/',
        TemplateView.as_view(template_name="portfolio/contact.html"),
        name="portfolio.contact"
    ),
]
