from django.shortcuts import render


def project(request, number):
    return render(request, f'portfolio/project_{number}.html', {})
