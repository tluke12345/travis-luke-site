from .base import *
import django_heroku


DEBUG = False
TEMPLATE_DEBUG = False
ALLOWED_HOSTS = []
SECURE_SSL_REDIRECT = True
# STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
# MIDDLEWARE = ['whitenoise.middleware.WhiteNoiseMiddleware'] + MIDDLEWARE

# Insert middleware just below django.security.SecurityMiddleware
# security_middleware_idx = MIDDLEWARE.index(
#     'django.middleware.security.SecurityMiddleware')
# new_middleware = MIDDLEWARE[:]
# new_middleware.insert(security_middleware_idx + 1,
#                       'whitenoise.middleware.WhiteNoiseMiddleware')
# MIDDLEWARE = new_middleware


DEBUG_PROPAGATE_EXCEPTIONS = True
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': ('%(asctime)s [%(process)d] [%(levelname)s] ' +
                       'pathname=%(pathname)s lineno=%(lineno)s ' +
                       'funcname=%(funcName)s %(message)s'),
            'datefmt': '%Y-%m-%d %H:%M:%S'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        }
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'testlogger': {
            'handlers': ['console'],
            'level': 'INFO',
        }
    }
}
# AUTH_PASSWORD_VALIDATORS = [
#     {
#         'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
#     },
# ]

# configure django app for Heroku
django_heroku.settings(locals())


# from memcacheify import memcacheifyfrom
# from postgresify import postgresify
# from S3 import CallingFormat


# ########## EMAIL CONFIGURATION
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = environ.get('EMAIL_HOST', 'smtp.gmail.com')
# EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD', '')
# EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER', 'your_email@example.com')
# EMAIL_PORT = environ.get('EMAIL_PORT', 587)
# EMAIL_SUBJECT_PREFIX = '[%s] ' % SITE_NAME
# EMAIL_USE_TLS = True
# SERVER_EMAIL = EMAIL_HOST_USER
# ########## END EMAIL CONFIGURATION


# ########## DATABASE CONFIGURATION
# DATABASES = postgresify()
# ########## END DATABASE CONFIGURATION


# ########## CACHE CONFIGURATION
# CACHES = memcacheify()
# ########## END CACHE CONFIGURATION


# ########## CELERY CONFIGURATION
# BROKER_TRANSPORT = 'amqplib'

# # Set this number to the amount of allowed concurrent connections on your AMQP
# # provider, divided by the amount of active workers you have.
# #
# # For example, if you have the 'Little Lemur' CloudAMQP plan (their free tier),
# # they allow 3 concurrent connections. So if you run a single worker, you'd
# # want this number to be 3. If you had 3 workers running, you'd lower this
# # number to 1, since 3 workers each maintaining one open connection = 3
# # connections total.
# #
# BROKER_POOL_LIMIT = 3
# BROKER_CONNECTION_MAX_RETRIES = 0
# BROKER_URL = environ.get('RABBITMQ_URL') or environ.get('CLOUDAMQP_URL')
# CELERY_RESULT_BACKEND = 'amqp'
# ########## END CELERY CONFIGURATION


# ########## S3 STORAGE CONFIGURATION
# INSTALLED_APPS += (
#     'storages',
# )

# STATICFILES_STORAGE = DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
# AWS_CALLING_FORMAT = CallingFormat.SUBDOMAIN
# AWS_ACCESS_KEY_ID = environ.get('AWS_ACCESS_KEY_ID', '')
# AWS_SECRET_ACCESS_KEY = environ.get('AWS_SECRET_ACCESS_KEY', '')
# AWS_STORAGE_BUCKET_NAME = environ.get('AWS_STORAGE_BUCKET_NAME', '')
# AWS_AUTO_CREATE_BUCKET = True
# AWS_QUERYSTRING_AUTH = False

# # AWS cache settings, don't change unless you know what you're doing:
# AWS_EXPIRY = 60 * 60 * 24 * 7
# AWS_HEADERS = {
#     'Cache-Control': 'max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRY,
#         AWS_EXPIRY)
# }

# # See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
# STATIC_URL = 'https://s3.amazonaws.com/%s/' % AWS_STORAGE_BUCKET_NAME
# ########## END STORAGE CONFIGURATION

# ########## ALLOWED HOSTS CONFIGURATION
# ALLOWED_HOSTS = ['.herokuapp.com']
# ########## END ALLOWED HOST CONFIGURATION
