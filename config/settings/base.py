import logging
import environ
import django
from django.utils.translation import gettext_lazy as _
from pathlib import Path

logging.getLogger().setLevel(logging.INFO)

BASE_DIR = Path(__file__).resolve().parent.parent.parent
ENV_PATH = BASE_DIR / '.env'

# SETUP environ
env = environ.Env()
environ.Env.read_env(str(ENV_PATH))

# Read Env settings
SECRET_KEY = env('SECRET_KEY')
DATABASES = {'default': env.db('DATABASE_URL')}

# Amazon S3 Media Storage
if env.bool('USE_S3'):
    AWS_ACCESS_KEY_ID = env.str('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = env.str('AWS_SECRET_ACCESS_KEY')
    AWS_STORAGE_BUCKET_NAME = env.str('AWS_STORAGE_BUCKET_NAME')

    # aws settings
    AWS_S3_CUSTOM_DOMAIN = f'{AWS_STORAGE_BUCKET_NAME}.s3.amazonaws.com'
    AWS_S3_OBJECT_PARAMETERS = {'CacheControl': 'max-age=86400'}

    # s3 static settings
    MEDIA_URL = f'https://{AWS_S3_CUSTOM_DOMAIN}/media/'
    DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'

else:
    MEDIA_ROOT = BASE_DIR / 'media'
    MEDIA_URL = env.str('MEDIA_URL', default='/media/')

# STATIC FILES
STATIC_ROOT = BASE_DIR / 'staticfiles'
STATIC_URL = env.str('STATIC_URL', default='/static/')
STATICFILES_DIRS = [BASE_DIR / 'static']

# APPS
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'
ROOT_URLCONF = 'config.urls'
WSGI_APPLICATION = 'config.wsgi.application'
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'wagtail.contrib.forms',
    'wagtail.contrib.redirects',
    'wagtail.embeds',
    'wagtail.sites',
    'wagtail.users',
    'wagtail.snippets',
    'wagtail.documents',
    'wagtail.images',
    'wagtail.search',
    'wagtail.admin',
    'wagtail.core',
    'modelcluster',
    'taggit',
    'django_filters',
    'storages',

    'apps.core',
    'apps.m',
    'apps.website',
    'apps.portfolio',
    'apps.eventManager',
    'apps.event',
    'apps.calendar',
    'apps.message',
    'apps.email',
    'apps.blog',
    'apps.issuetracker',
    'apps.hourtracker',
    'apps.keyboard',

]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'wagtail.contrib.redirects.middleware.RedirectMiddleware',
]
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            'templates',
            # Django built-in form templates
            django.__path__[0] + '/forms/templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
            ],
        },
    },
]

# Form renderer using 'TemplateSettings' allows for custom widget templates
# it is necessary to add built-in django templates to TEMPLATES['DIRS']
FORM_RENDERER = 'django.forms.renderers.TemplatesSetting'

# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/
# NOTE: import django.utils.translation.gettext_lazy not gettext
LANGUAGES = [
    ('ja', _('Japanese')),
    ('en', _('English')),
]
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Tokyo'
USE_I18N = True
USE_L10N = True
USE_TZ = True
LOCALE_PATHS = (
    BASE_DIR / 'translations',
)

WAGTAIL_SITE_NAME = 'travis-luke.com'

# Django 3.2 - set default index field
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
