from .base import *

DEBUG = True
TEMPLATE_DEBUG = True
TEST = 'asdf'

ALLOWED_HOSTS = [
    "*"
]

# Debug email
# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.sqlite3',
#         'NAME': normpath(join(DJANGO_ROOT, 'default.db')),
#         'USER': '',
#         'PASSWORD': '',
#         'HOST': '',
#         'PORT': '',
#     }
# }

# In-memory Cache
# CACHES = {
#   'default': {
#     'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
#   }
# }

# Celery
# CELERY_ALWAYS_EAGER = True
# CELERY_EAGER_PROPAGATES_EXCEPTIONS = True

# debug toolbar
# INSTALLED_APPS += ('debug_toolbar')
# INTERNAL_IPS = ('1.2.0.0.1',)
# MIDDLEWARE += ('debug_toolbar.middleware.DebugToolbarMiddleware',)
# DEBUG_TOOLBAR_CONFIG = {
#   'INTERCEPT_REDIRECTS': False,
# }
