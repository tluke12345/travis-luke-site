from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.urls import include

urlpatterns = [
    path(
        'i18n/',
        include('django.conf.urls.i18n')
    ),
    path(
        'a/',
        admin.site.urls
    ),
    path(
        'm/',
        include('apps.m.urls')
    ),
    path(
        'em/',
        include('apps.eventManager.urls')
    ),
    path(
        'tracker/',
        include('apps.issuetracker.urls')
    ),
    path(
        'hours/',
        include('apps.hourtracker.urls')
    ),
    path(
        'keyboard/',
        include('apps.keyboard.urls')
    )


    # LOCALIZED PAGES
] + i18n_patterns(
    path(
        'accounts/login/',
        auth_views.LoginView.as_view(),
        name="login"
    ),
    path(
        'accounts/logout/',
        auth_views.LogoutView.as_view(),
        {'next_page': '/'},
        name="logout"
    ),
    path(
        'portfolio/',
        include('apps.portfolio.urls')
    ),
    path(
        '',
        include('apps.website.urls')
    ),

    # WAGTAIL
    path(
        'cms/',
        include('wagtail.admin.urls'),
    ),
    path(
        'documents/',
        include('wagtail.documents.urls')
    ),

    # This points to the 'root page'
    # see notes->django->wagtail for details
    path(
        '',
        include('wagtail.core.urls')
    ),
)

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
